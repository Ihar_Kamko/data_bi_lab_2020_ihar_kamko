# 6. File reading 
#T ask: change script from previous task, now input parameter will be path to text file

import argparse

parser = argparse.ArgumentParser(description='')
parser.add_argument('x',type=str)
filepath = parser.parse_args()

def factorial(n):
    if n == 0:
        return 1
    else:
        return n * factorial(n - 1)

with open(filepath.x) as file_object:
    for line in file_object:    
        try:
            print(factorial(int(line))) 
        except ValueError:
            continue    # i thought that skip means omit value

