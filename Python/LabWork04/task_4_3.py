# 3. Error handling part 1.
# Task: change script from previous task to handle type cast errors, 
# for example when user passed string “123r” need to print message instead of error trace.
import sys

print("Factorial calculation.")

def factorial(n):
    if n == 0:
        return 1
    else:
        return n * factorial(n - 1)

try:
    x = int(sys.argv[1])
    print(factorial(x)) 
except ValueError:
    print('Please use only digits!')
