# 4.Date functions, int limitation.
# Task: change script from previous task to calculate duration of calculation, 
# add print to console message with datetime when execution started and completed. 
# Test script on big values. Try to findinteger value limitation for result.

import sys, datetime

def factorial(n):
    if n == 0:
        return 1
    else:
        return n * factorial(n - 1)

print("Factorial calculation.")
try:
    x = int(sys.argv[1])
    a=datetime.datetime.now()
    print("The result is:")  
    print(factorial(x)) 
    print("\nStart of execution:")
    print(a)
    b=datetime.datetime.now()
    print("End of execution:")
    print(b)
    print("\nExecution time:")
    print(b-a)
except ValueError:
    print('Please use only digits!')
