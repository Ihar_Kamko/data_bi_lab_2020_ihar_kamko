# 2. Positional command line arguments, type cast
# To add reference to additional module Python use keyword import <module name>
# Task: change script from previous task to use script positional parameter instead of hardcode. 

import sys

print("Factorial calculation.")

def factorial(n):
    if n == 0:
        return 1
    else:
        return n * factorial(n - 1)

print("The result is:")  

x = int(sys.argv[1])
print(factorial(x))