# 1. Task:Write a script to calculate factorial of number. 
# Input value can be hardcoded at the beginning of script. 
# Use recursion function for this task.

print("Factorial calculation.")
x = int(input("Please enter a number: "))

def factorial(n):
    if n == 0:
        return 1
    else:
        return n * factorial(n - 1)
 
print("The result is:")      
print(factorial(x)) 


