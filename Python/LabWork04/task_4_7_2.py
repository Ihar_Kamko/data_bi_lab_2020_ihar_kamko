# Task 7.2: output file encoding is UTF without BOM, new line char is \r\n

import argparse

parser = argparse.ArgumentParser(description='')
parser.add_argument('x', type=str)
parser.add_argument('y', type=str)
filepath = parser.parse_args()

def factorial(n):
    if n == 0:
        return 1
    else:
        return n * factorial(n - 1)
    
f=open(filepath.x)
abc=f.readlines()


with open(filepath.y, 'w', encoding='utf-8') as output_file:
    for line in abc:    
        try:
            z=line.strip()
            if line in abc[0]:
                output_file.write(z+'_result \n')
            else:
                output_file.write(z +',' +str(factorial(int(z)))+'\n')
        except ValueError:
            output_file.write(z+',\n')
        