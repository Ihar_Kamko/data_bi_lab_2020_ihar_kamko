# 5. Named arguments, argparse
# Task: modify previous script, replace positional argument by named, use argparse module for this task 

import datetime, argparse

parser = argparse.ArgumentParser(description='')
parser.add_argument('x', type=int)
argument = parser.parse_args()

def factorial(n):
    if n == 0:
        return 1
    else:
        return n * factorial(n - 1)

print("Factorial calculation.")
try:
    a=datetime.datetime.now()
    print("The result is:")  
    print(factorial(argument.x)) 
    print("\nStart of execution:")
    print(a)
    b=datetime.datetime.now()
    print("End of execution:")
    print(b)
    print("\nExecution time:")
    print(b-a)
except ValueError:
    print('Please use only digits!')

