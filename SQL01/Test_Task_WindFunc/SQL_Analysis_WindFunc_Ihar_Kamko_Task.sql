--Build the query to generate a report about regions with the maximum number of products sold (quantity_sold) for each channel for the entire period.
WITH abc AS (
SELECT  c2.channel_desc, 
		c3.country_region, 
		sum(quantity_sold) AS quantity, 
		sum(sum(quantity_sold)) OVER (PARTITION BY c2.channel_desc) AS summary,
		rank() OVER (PARTITION BY channel_desc ORDER BY sum(quantity_sold) DESC)
FROM sales s 
	INNER JOIN customers c ON s.cust_id =c.cust_id 
	INNER JOIN channels c2 ON s.channel_id =c2.channel_id 
	INNER JOIN countries c3 ON c.country_id =c3.country_id
GROUP BY  c2.channel_desc, c3.country_region
ORDER BY c2.channel_desc, quantity DESC)

SELECT  channel_desc, 
		country_region, 
		to_char(quantity, '999999999.99') AS sales, 
		to_char(quantity*100/summary, '999.99 %') AS "SALES %"
from abc
where rank =1
order by sales desc;


--2. Define subcategories of products (prod_subcategory) for which sales for 1998-2001 have always been higher (sum(amount_sold)) compared to the previous year. 
--The final dataset must include only one column (prod_subcategory).
WITH abc AS (
SELECT p.prod_subcategory, t.calendar_year, sum(amount_sold),
	rank() OVER (PARTITION BY p.prod_subcategory ORDER BY t.calendar_year asc) AS year_rank,
	rank() OVER (PARTITION BY p.prod_subcategory ORDER BY sum(amount_sold) asc) AS amount_rank
FROM sales s 
	INNER JOIN products p ON s.prod_id =p.prod_id 
	INNER JOIN times t ON s.time_id=t.time_id 
WHERE t.calendar_year BETWEEN '1998' AND '2001'
GROUP BY p.prod_subcategory, t.calendar_year
ORDER BY p.prod_subcategory, t.calendar_year ASC)

SELECT prod_subcategory
FROM abc 
WHERE year_rank=amount_rank
GROUP BY prod_subcategory
HAVING count(prod_subcategory)=4;