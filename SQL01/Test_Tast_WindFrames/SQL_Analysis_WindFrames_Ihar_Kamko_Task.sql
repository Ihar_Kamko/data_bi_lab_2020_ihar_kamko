--Build the query to generate sales report for 1999 and 2000 in the context of quarters and product categories. 
--In the report you should analyze the sales of products from the categories 'Electronics', 'Hardware' and 'Software/Other', through the channels 'Partners' and 'Internet':
WITH abc AS
	(SELECT t.calendar_year, 
	t.calendar_quarter_desc, p.prod_category, 
	to_char(sum(amount_sold),  '999,999,999.99') as sales$,
	round((sum(amount_sold) - (first_value(sum(amount_sold)) OVER w))*100/first_value(sum(amount_sold)) OVER w, 2) AS diff_percent,
	to_char(sum(sum(amount_sold)) OVER (PARTITION BY t.calendar_year ORDER BY t.calendar_quarter_desc RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW), '999,999,999.00') AS cum_sum$
FROM sales s
	INNER JOIN channels c ON s.channel_id =c.channel_id
	INNER JOIN products p ON s.prod_id =p.prod_id
	INNER JOIN times t ON s.time_id =t.time_id 
WHERE c.channel_desc IN ('Partners', 'Internet')
AND p.prod_category IN ('Electronics', 'Hardware', 'Software/Other')
AND t.calendar_year IN ('1999', '2000')
GROUP BY t.calendar_year, t.calendar_quarter_desc, p.prod_category
WINDOW w AS (PARTITION BY t.calendar_year, p.prod_category ORDER BY t.calendar_quarter_desc ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW EXCLUDE CURRENT ROW)) 

SELECT  calendar_year, calendar_quarter_desc, prod_category, sales$, 
		CASE
			WHEN diff_percent IS NULL THEN 'N/A'
			ELSE to_char(diff_percent, '999.99%')
		END AS diff_percent,
		cum_sum$
FROM abc
ORDER BY calendar_year, calendar_quarter_desc ASC, sales$ desc;