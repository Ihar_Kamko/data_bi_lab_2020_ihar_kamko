--TASK 1
--Analyze annual sales by channels and regions. Build the query to generate the same report:
WITH abc AS (
SELECT country_region, calendar_year, channel_desc, to_char(round(amount_sold, 0), '99,999,999 $') AS amount_sold, 
	round(period_pecentage, 2) AS  percentage_by_channels,
	round(period_pecentage - first_value(amount_sold*100/year_sum) OVER (PARTITION BY country_region, channel_desc ORDER BY calendar_year ASC 
						ROWS BETWEEN 1 PRECEDING AND CURRENT ROW EXCLUDE CURRENT ROW), 2) AS percentage_previous_period
FROM
	(SELECT c2.country_region, t.calendar_year, c3.channel_desc, sum(amount_sold) AS amount_sold,
	sum(sum(amount_sold)) OVER w AS year_sum,
	sum(amount_sold)*100/sum(sum(amount_sold)) OVER w AS period_pecentage
	FROM sales s 
		INNER JOIN customers c ON s.cust_id =c.cust_id 
		INNER JOIN countries c2 ON c.country_id =c2.country_id 
		INNER JOIN channels c3 ON s.channel_id =c3.channel_id
		INNER JOIN times t ON s.time_id =t.time_id 
	GROUP BY c2.country_region, t.calendar_year, c3.channel_desc
	WINDOW w AS (PARTITION BY c2.country_region, t.calendar_year ORDER BY t.calendar_year ASC)
	ORDER BY c2.country_region, t.calendar_year, c3.channel_desc) AS abc )

SELECT country_region, calendar_year, channel_desc, amount_sold, to_char(percentage_by_channels, '99.99 %') AS "% BY CHANNELS", 
	to_char(percentage_by_channels-percentage_previous_period, '99.99 %') AS "% PREVIOUS PERIOD", 
	to_char(percentage_previous_period, '99.99 %') AS "% DIFF"
FROM abc
WHERE calendar_year IN (1999, 2000, 2001)
AND country_region IN ('Americas', 'Asia', 'Europe')
ORDER BY country_region, calendar_year, channel_desc;


--TASK 2
--Build the query to generate a sales report for the 49th, 50th and 51st weeks of 1999. Add column CUM_SUM for accumulated amounts within weeks. 
--For each day, display the average sales for the previous, current and next days (centered moving average, CENTERED_3_DAY_AVG column).
--For Monday, calculate average weekend sales + Monday + Tuesday. For Friday, calculate the average sales for Thursday + Friday + weekends.
SELECT t.calendar_week_number, t.time_id, t.day_name, to_char(sum(amount_sold), '99999999.99') AS sales,
		to_char(sum(sum(amount_sold)) OVER (PARTITION BY t.calendar_week_number ORDER BY t.day_number_in_week RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW), '99999999.99') AS cum_sum,
		to_char(avg(sum(amount_sold)) OVER (ORDER BY s.time_id RANGE BETWEEN interval '1 day' PRECEDING AND '1 day' FOLLOWING), '99999999.99') AS centered_3_day_avg
FROM sales s 
	INNER JOIN times t ON s.time_id =t.time_id
WHERE t.calendar_week_number in (49, 50, 51)
AND t.calendar_year = 1999
GROUP BY t.calendar_week_number, t.time_id, t.day_name, t.day_number_in_week, s.time_id
ORDER BY t.calendar_week_number, t.day_number_in_week;



--TASK 3
--Prepare 3 examples of using window functions with a frame clause (RANGE, ROWS, and GROUPS modes)
--Explain why you used a particular type of frame in each example. It can be one query or 3 separate queries.

--So as it is task for task to explain understanding of the theory I prepare this one.

--For the better demonstration of difference we can do something like this
WITH abc AS (
SELECT c.channel_desc, p.prod_desc, round(sum(amount_sold)/1000, 0) as amount
FROM sales s 
	INNER JOIN products p ON s.prod_id =p.prod_id
	INNER JOIN channels c ON s.channel_id =c.channel_id
GROUP BY p.prod_desc, c.channel_desc)

SELECT *,
		sum(amount) OVER (ORDER BY amount ASC ROWS BETWEEN '1' PRECEDING AND CURRENT ROW) AS rows,
--ROWS gives just sum between value and 1 priop
		sum(amount) OVER (ORDER BY amount ASC RANGE BETWEEN '1' PRECEDING AND CURRENT ROW) AS range,
		sum(amount) OVER (ORDER BY amount ASC GROUPS BETWEEN '1' PRECEDING AND CURRENT ROW) AS groups
--RANGE and GROUPS gives the same result till first difference in column 'amount' >1 it's '9' value
--GROUPS gives 9(18)+6(12)=30; RANGE gives only 18, cause difference between 9 and 6 more than 1
FROM abc	
GROUP BY prod_desc, channel_desc, amount
ORDER BY amount asc
