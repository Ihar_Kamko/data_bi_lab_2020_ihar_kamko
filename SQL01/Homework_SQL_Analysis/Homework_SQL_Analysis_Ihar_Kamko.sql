--1)	which products brought to the company the greatest net profit
SELECT pr.prod_name , sum(amount_sold)-sum(total_cost) AS total_revenues
FROM profits p 
INNER JOIN products pr ON p.prod_id =pr.prod_id 
GROUP BY pr.prod_name 
ORDER BY total_revenues DESC
LIMIT 5;

--2) what type of advertising is most beneficial?
SELECT p.promo_category, COALESCE(sum(s.amount_sold),0) - max(promo_cost) as profit
FROM promotions p
LEFT JOIN sales s ON p.promo_id =s.promo_id 
WHERE p.promo_cost <>0
GROUP BY p.promo_category
ORDER BY profit DESC

--3) which way the company sell goods in larger quantities?
SELECT c.channel_desc, coalesce(count(*), 0) as total_sales
FROM sales s
INNER JOIN channels c ON s.channel_id =c.channel_id  
GROUP BY c.channel_desc
ORDER BY total_sales desc

--additional questions:
--4) income statistics by month, ordered by sequence of months
WITH abc AS (
	SELECT rank() OVER (ORDER BY EXTRACT(MONTH FROM time_id)) AS number,to_char(time_id, 'Month') AS month, amount_sold
	from sales s) 

select month, sum(amount_sold) 
FROM abc 
GROUP BY month, number
ORDER BY number

--4.1 divided by regions
WITH abc AS (
	SELECT rank() OVER (ORDER BY EXTRACT(MONTH FROM time_id)) AS number,to_char(time_id, 'Month') AS month, amount_sold, c2.country_region 
	FROM sales s
	INNER JOIN customers c ON s.cust_id =c.cust_id 
	INNER JOIN countries c2 ON c.country_id =c2.country_id) 

select country_region, month, sum(amount_sold) 
FROM abc 
GROUP BY month, number, country_region
ORDER BY country_region, number


--5) what kind of products more often than others bought by widows and divorced
with abc as (
select *, 
	case
		when c.cust_marital_status like 'divor%' then 'Divorced' 
		when c.cust_marital_status like 'widow%' then 'Widowed'
	end as status
from sales s 
inner join customers c on s.cust_id =c.cust_id
inner join products p on s.prod_id =p.prod_id
where c.cust_marital_status like 'divor%' or c.cust_marital_status like 'widow%')

select prod_name, 
sum(cast(status ='Divorced' AS int)) AS Divorced,							
sum(cast(status ='Widowed' AS int)) AS Widowed	
from abc
group by prod_name
order by divorced desc
limit 10


--6) monthly changes in sales volumes
create or replace view monthly as 		
with abc as(		
select extract(YEAR from time_id) as year, to_char(time_id, 'Month') as month, rank() OVER (ORDER BY extract(YEAR from time_id), EXTRACT(MONTH FROM time_id)) AS number
from sales s)

select number, year, month, count(*) as count
from abc
group by year, month, number
order by number asc

select year, month,
	round((lead(count) OVER w - count)::decimal(6,2)*100/count,2)
from monthly
WINDOW w AS (ORDER by number asc)

