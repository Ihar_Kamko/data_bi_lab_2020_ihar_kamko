-- trends_analisys.currency definition

-- Drop table

-- DROP TABLE trends_analisys.currency;

CREATE TABLE trends_analisys.currency (
	currency_id smallserial NOT NULL,
	currency_initials varchar(3) NOT NULL,
	currency_description varchar(100) NOT NULL,
	CONSTRAINT currency_pkey PRIMARY KEY (currency_id)
);