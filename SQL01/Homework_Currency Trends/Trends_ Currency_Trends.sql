--1) Calculation of average daily fluctuations of all currency, AND total percent for each currency
WITH fluctuations AS (SELECT  target_currency_id, 
    ((lag(rate) OVER w - rate)*100/rate)  AS	daily_fluctuation	--lag function allows go through values one at a time
FROM rates_new
WINDOW w AS (partition by target_currency_id ORDER by date_of_rate desc))

select c.currency_initials, c.currency_description, avg(abs(daily_fluctuation))::decimal(5,3) as average_daily_fluctuations, sum(daily_fluctuation)::decimal(7,3) as total_percent_increase 
from fluctuations
	INNER JOIN currency c ON fluctuations.target_currency_id=c.currency_id
group by c.currency_initials, c.currency_description
order by total_percent_increase desc;

--2) Yearly growth of the currency
WITH fluctuations as (SELECT  c.currency_description, r.date_of_rate,
	 ((lead(rate) OVER w - rate)*100/rate)::decimal(7,2)  AS percent_growth,
	EXTRACT(YEAR FROM r.date_of_rate) AS YEAR, (EXTRACT (MONTH FROM r.date_of_rate), EXTRACT(DAY FROM r.date_of_rate)) as values
FROM rates_new r 
		INNER JOIN currency c ON r.target_currency_id=c.currency_id
		WHERE ((EXTRACT (MONTH FROM r.date_of_rate), EXTRACT(DAY FROM r.date_of_rate)) = (12,31)
		or (EXTRACT (MONTH FROM r.date_of_rate), EXTRACT(DAY FROM r.date_of_rate)) = (1,1))
		or r.date_of_rate in (SELECT max(rates_new.date_of_rate) FROM rates_new)
WINDOW w AS (partition by target_currency_id ORDER by date_of_rate asc)
ORDER BY date_of_rate asc)

select
	year, currency_description, percent_growth
FROM fluctuations 
	where 	((EXTRACT (MONTH from date_of_rate), EXTRACT(DAY FROM date_of_rate)) = (1,1)
	or  date_of_rate=(SELECT max(date_of_rate) FROM fluctuations))
	and (EXTRACT (MONTH from date_of_rate), EXTRACT(DAY FROM date_of_rate)) not in 
		(select EXTRACT (MONTH FROM date_of_rate), EXTRACT(DAY FROM date_of_rate) 
		from fluctuations 
		where date_of_rate in (SELECT max(date_of_rate) FROM fluctuations))
ORDER BY year, currency_description asc;

--3) all currency fluctuaions on zodiac periods AND total increasing percent
WITH fluctuations AS (SELECT  rates_new.*,
    (lag(rate) OVER w - rate)*100/rate  AS percent,  zs.sign_name as zodiac
FROM rates_new
	INNER JOIN zodiac_signs zs ON (EXTRACT(MONTH FROM date_of_rate), EXTRACT(DAY FROM date_of_rate)) 
	BETWEEN (EXTRACT(MONTH FROM zs.sign_FROM), EXTRACT(DAY FROM zs.sign_FROM))	AND (EXTRACT(MONTH FROM zs.sign_to), EXTRACT(DAY FROM zs.sign_to))
	WINDOW w AS (partition by target_currency_id ORDER by date_of_rate desc))

SELECT zodiac, (avg(abs(percent)))::decimal(5,3) as daily_fluctuations, sum(percent)::decimal(7,3) as zodiac_total_positive_percent
FROM fluctuations
GROUP  BY zodiac
ORDER BY daily_fluctuations DESC;


--4) daily fluctuations depending on moon phase and daily average percent of fluctuations
with fluctuations as (
	select 
	target_currency_id, row_number() over (partition by target_currency_id order by date_of_rate ) as id, date_of_rate,
	(lag(rate) OVER w - rate)*100/rate AS daily_fluctuation
from rates_new 			
WINDOW w AS (partition by target_currency_id ORDER by date_of_rate desc)
order by date_of_rate asc 
	) 
select
	case
    	when (id-1)*24%708 between 0 and 36 then 'New Moon'				
		when (id-1)*24%708 between 37 and 354 then 'First-quarter'
		when (id-1)*24%708 between 355 and 390 then 'Full moon'
		when (id-1)*24%708 between 391 and 708 then 'Third-quarter'
	end as moon_phases,
	avg(abs(daily_fluctuation))::decimal(5,3) as daily_fluctuations,
	((sum(daily_fluctuation))*16/count(*))::decimal(9,3) as phases_average_positive_percent
FROM fluctuations
GROUP  BY moon_phases
ORDER BY daily_fluctuations DESC;
