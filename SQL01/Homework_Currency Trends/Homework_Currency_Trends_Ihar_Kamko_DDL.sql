CREATE SCHEMA trends_analisys;

SET search_path TO trends_analisys;

CREATE TABLE currency
(
	currency_id 			smallserial PRIMARY KEY,
	currency_initials		varchar(3) NOT NULL,
	currency_description 	varchar(100) NOT NULL
);

CREATE TABLE rates_new 
(
	date_of_rate date NOT NULL,
	base_currency_id int4 NOT NULL,
	target_currency_id int4 NOT NULL,
	rate numeric(12,6) NULL,
	CONSTRAINT positive_rate CHECK ((rate > (0)::numeric)),
	CONSTRAINT rates_new_pkey PRIMARY KEY (date_of_rate, base_currency_id, target_currency_id)
);

ALTER TABLE rates_new ADD CONSTRAINT rates_new_target_currency_id_fkey FOREIGN KEY (target_currency_id) REFERENCES trends_analisys.currency(currency_id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE rates_new ADD CONSTRAINT rates_new_base_currency_id_fkey FOREIGN KEY (base_currency_id) REFERENCES trends_analisys.currency(currency_id) ON UPDATE CASCADE ON DELETE RESTRICT;

CREATE TABLE zodiac_signs
(	
	sign_id			smallserial PRIMARY KEY,	
	sign_name		varchar(30) NOT NULL,
	sign_from		date NOT NULL,
	sign_to			date NOT NULL
);

CREATE TABLE moon_phases
	(phase_id			smallserial PRIMARY KEY,
	 phase_description	varchar(50) NOT NULL,
	 phase_duration		interval HOUR NOT NULL
);




--drop schema trends_analisys cascade
