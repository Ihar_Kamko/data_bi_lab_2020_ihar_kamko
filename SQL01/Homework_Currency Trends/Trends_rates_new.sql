-- trends_analisys.rates_new definition

-- Drop table

-- DROP TABLE trends_analisys.rates_new;

CREATE TABLE trends_analisys.rates_new (
	date_of_rate date NOT NULL,
	base_currency_id int4 NOT NULL,
	target_currency_id int4 NOT NULL,
	rate numeric(12,6) NULL,
	CONSTRAINT positive_rate CHECK ((rate > (0)::numeric)),
	CONSTRAINT rates_new_pkey PRIMARY KEY (date_of_rate, base_currency_id, target_currency_id)
);


-- trends_analisys.rates_new foreign keys

ALTER TABLE trends_analisys.rates_new ADD CONSTRAINT rates_new_base_currency_id_fkey FOREIGN KEY (base_currency_id) REFERENCES trends_analisys.currency(currency_id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE trends_analisys.rates_new ADD CONSTRAINT rates_new_target_currency_id_fkey FOREIGN KEY (target_currency_id) REFERENCES trends_analisys.currency(currency_id) ON UPDATE CASCADE ON DELETE RESTRICT;