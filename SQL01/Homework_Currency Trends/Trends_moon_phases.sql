-- trends_analisys.moon_phases definition

-- Drop table

-- DROP TABLE trends_analisys.moon_phases;

CREATE TABLE trends_analisys.moon_phases (
	phase_id smallserial NOT NULL,
	phase_description varchar(50) NOT NULL,
	phase_duration interval NOT NULL,
	CONSTRAINT moon_phases_pkey PRIMARY KEY (phase_id)
);