-- trends_analisys.zodiac_signs definition

-- Drop table

-- DROP TABLE trends_analisys.zodiac_signs;

CREATE TABLE trends_analisys.zodiac_signs (
	sign_id smallserial NOT NULL,
	sign_name varchar(30) NOT NULL,
	sign_from date NOT NULL,
	sign_to date NOT NULL,
	CONSTRAINT zodiac_signs_pkey PRIMARY KEY (sign_id)
);