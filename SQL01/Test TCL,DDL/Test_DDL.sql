create   SCHEMA medical_institutions; -- cascade;

SET search_path TO medical_institutions;

CREATE TABLE districts
(	
	district_id						smallserial PRIMARY KEY,
	distict_name					varchar(50) NOT NULL
);

CREATE TABLE streets 
(
	street_id						smallserial PRIMARY KEY,
	street_name						varchar(50) NOT NULL 
);

CREATE TABLE districts_streets 
(
	district_id						smallint references districts on delete restrict on update CASCADE,
	street_id						smallint references streets on delete restrict on update cascade,
	primary key 					(district_id, street_id)
);

CREATE TABLE addresses
(
	adress_id						smallserial PRIMARY KEY,
	district_id						smallint NOT NULL,
	street_id						smallint NOT NULL,
	building_number					varchar(5),
	FOREIGN KEY (district_id, street_id) REFERENCES districts_streets  (district_id, street_id)
);

CREATE TABLE medical_services
(
	medical_service_id 				smallserial PRIMARY KEY,
	medical_service_description 	varchar(50) NOT NULL
);

CREATE TABLE medical_institutions
(
	medical_institution_id 			smallserial PRIMARY KEY,
	medical_institution_name		varchar(50) NOT NULL, 
	address_id						smallint NOT null references addresses  on delete restrict on update cascade,
	staff_quantity					smallint NOT NULL DEFAULT 90, 
	staffing						smallint NOT NULL
);

CREATE TABLE staff 
(
	staff_id						smallserial PRIMARY KEY,
	staff_specialization			varchar(50) NOT NULL,
	medical_institution_id			smallint NOT null references medical_institutions on delete restrict on update CASCADE,
	staff_first_name 				varchar(50) NOT NULL, 
	staff_last_name 				varchar(50) NOT NULL, 
	address_id						smallint  NOT null references addresses  on delete restrict on update cascade,
	age 							smallint NOT null,
	constraint age_positive check (age>18),
	unique(staff_id, medical_institution_id)
);

CREATE TABLE patients
(
	patient_id						smallserial PRIMARY KEY,  
	patient_first_name 				varchar(50) NOT NULL, 
	patient_last_name 			varchar(50) NOT NULL, 
	address_id						smallint  NOT null references addresses  on delete restrict on update cascade,
	age 							smallint NOT NULL
);

CREATE TABLE services_institutions
(
	medical_institution_id 			smallint references medical_institutions on delete restrict on update CASCADE,
	medical_service_id				smallint references medical_services  on delete restrict on update cascade,
	primary key 					(medical_institution_id, medical_service_id)
);

CREATE TABLE patient_visits
(
	visit_id						smallserial PRIMARY KEY,
	date_of_visit					date not null default current_date,
	medical_institution_id 			smallint references medical_institutions on delete restrict on update CASCADE,
	staff_id						smallint references staff  on delete restrict on update cascade,
	patient_id						smallint references patients on delete restrict on update cascade,
	diagnosis						varchar(300) NOT NULL DEFAULT 'Healthy. Simulates',
	recipe							varchar(300),
	constraint date_legal check (date_of_visit <=current_date),
	FOREIGN KEY (medical_institution_id, staff_id) REFERENCES staff  (medical_institution_id, staff_id)
);




