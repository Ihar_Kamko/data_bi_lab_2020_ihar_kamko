select s.staff_id, concat(s.staff_first_name||' '||s.staff_last_name)
from staff s
left join
(select s.staff_id, count(*) as number_visits from  patient_visits pv 
right join staff s on pv.staff_id =s.staff_id
where date_of_visit>'2020-07-31'
group by s.staff_id) as count_visits
on s.staff_id =count_visits.staff_id
where number_visits <5 or number_visits is null

