--Choose your top-3 favorite movies and add them to 'film' table. Fill rental rates with 4.99, 9.99 and 19.99 and rental durations with 1, 2 and 3weeks respectively

INSERT INTO film (title, release_year, language_id, rental_duration, rental_rate, replacement_cost )
VALUES ('The Lord of The Rings: The Return of the King', 2003, 1, 7, 4.99, 19.99),							--inserting 1st 
 	   ('The Green Mile', 1999, 1, 14, 9.99, 24.99),														--2nd
 	   ('The Shawshank Redemption', 1994, 1, 21, 19.99, 29.99)												--3rd film
RETURNING *;																								--returning result
--I set default number 1 to language_id column, so need to update it with language table to set language  'English'
UPDATE film f
SET language_id =l.language_id 
FROM language l
WHERE UPPER(l.name)='ENGLISH'
AND UPPER(f.title) IN ('THE LORD OF THE RINGS: THE RETURN OF THE KING', 'THE GREEN MILE', 'THE SHAWSHANK REDEMPTION');

--Add actors who play leading roles in your favorite movies to 'actor' and 'film_actor' tables (6 or more actors in total)
INSERT INTO actor  (first_name, last_name)																	--inserting actors in table actor
VALUES ('ELIJAH', 'WOOD'),
	   ('VIGGO','MORTENSEN'), 
	   ('IAN','MCKELLEN'), 
	   ('THOMAS','HANKS'),
	   ('MICHAEL','DUNCAN'),
	   ('TIMOTHY','ROBBINS'),
	   ('MORGAN','FREEMAN')
RETURNING *;																								--returning result
--inserting relations between films and actors
insert into film_actor
select a.actor_id, f.film_id 
from actor a, film f 
where UPPER(concat(a.first_name,' ',a.last_name)) in ('ELIJAH WOOD', 'VIGGO MORTENSEN', 'IAN MCKELLEN')
and UPPER(f.title) in ('THE LORD OF THE RINGS: THE RETURN OF THE KING')
or UPPER(concat(a.first_name,' ',a.last_name)) in ('THOMAS HANKS', 'MICHAEL DUNCAN')
and UPPER(f.title) in ('THE GREEN MILE')
or UPPER(concat(a.first_name,' ',a.last_name)) in ('TIMOTHY ROBBINS', 'MORGAN FREEMAN')
and UPPER(f.title) in ('THE SHAWSHANK REDEMPTION')
returning *;

--Add your favorite movies to any store's inventory.
insert into inventory (film_id, store_id)
select f.film_id, s.store_id 
from film f, store s
where UPPER(f.title) IN ('THE LORD OF THE RINGS: THE RETURN OF THE KING', 'THE GREEN MILE', 'THE SHAWSHANK REDEMPTION')
and s.store_id in 																								--i've decide to add some random value cause task says 'any store'
	(SELECT store_id 
	FROM store  
	ORDER BY random()
	LIMIT 1);		
	
--Alter any existing customer in the database who has at least 43 rental and 43 payment records. Change his/her personal data to yours (first name,last name, address, etc.). 
--Do not perform any updates on 'address' table, as it can impact multiple records with the same address. Change customer's create_date value to current_date

--first of all I need create my own adress 
insert into city (city, country_id)
select 'Homel', c.country_id 							--inserting Homel to city table
from country c
where upper(c.country) ='BELARUS'

insert into address (address, address2, district, city_id, phone)			--inserting my own address
select 'Checherskaya 22', '33', 'Volotova', c.city_id, 'not available'
from city c 
where upper(city)='HOMEL'
returning *;																--returning result

--then I need to update one of customers 
update customer 
set first_name = 'IHAR', 
	last_name = 'KAMKO',
	email = 'IHAR.KAMKO@gmail.com',
	create_date = current_date,
	address_id = 
		(select address_id from address where UPPER(concat(address,' ',address2))='CHECHERSKAYA 22 33')			--selecting address_id from address table
where customer_id in 																							--selecting one random customer which had at least 43 rental and 43 payment record
	(
		(select c.customer_id
			from rental r 
				inner join customer c on r.customer_id =c.customer_id 
				inner join payment p on r.rental_id =p.rental_id 
			group by c.customer_id
			having count(r.rental_id)>=43 and count(p.payment_id)>=43)
		order by random() 
		limit 1)
returning *;																									--returning result of update

--Remove any records related to you (as a customer) from all tables except 'Customer' and 'Inventory'
delete from payment p 											--deleting data from payment table cause it have FK to table rental
where p.customer_id = 
	(select customer_id 
	from customer 
	where upper(concat(first_name,' ', last_name))='IHAR KAMKO')
returning *;

delete from rental r											--deleting data from rental table
where r.customer_id = 
	(select customer_id 
	from customer 
	where upper(concat(first_name,' ', last_name))='IHAR KAMKO')
returning *;

--Rent you favorite movies from the store they are in and pay for them (add corresponding records to the database to represent this activity)
--inserting info into rental table
insert into rental (rental_date, inventory_id, customer_id, staff_id)
select current_timestamp, (
	   select inventory_id 
	   from inventory 
			where film_id in (
				select film_id 
				from film f 
				where upper(f.title) in ('THE LORD OF THE RINGS: THE RETURN OF THE KING'))), 
	   (select customer_id 
			from customer 
			where  upper(concat(first_name,' ', last_name))='IHAR KAMKO'),
	   3																							--cause seller the one who inserting that data about customer and film name - he knew his own ID for program
returning *;

insert into rental (rental_date, inventory_id, customer_id, staff_id)
select current_timestamp, (
	   select inventory_id 
	   from inventory 
			where film_id in (
				select film_id 
				from film f 
				where upper(f.title) in ('THE GREEN MILE'))), 
	   (select customer_id 
			from customer 
			where  upper(concat(first_name,' ', last_name))='IHAR KAMKO'),
	   3																							
returning *;

insert into rental (rental_date, inventory_id, customer_id, staff_id)
select current_timestamp, (
	   select inventory_id 
	   from inventory 
			where film_id in (
				select film_id 
				from film f 
				where upper(f.title) in ('THE SHAWSHANK REDEMPTION'))), 
	   (select customer_id 
			from customer 
			where  upper(concat(first_name,' ', last_name))='IHAR KAMKO'),
	   3																							
returning *;


--creating partition for inserting current date
CREATE TABLE public.payment_p2020_07 PARTITION OF public.payment 									--there was endless transaction when I added 31 July - 31 December partition, so I addad small period
FOR VALUES FROM ('2020-07-29 00:00:00+03') TO ('2020-08-07 00:00:00+03');

--inserting data into payment table
insert into payment (customer_id, staff_id, rental_id, amount, payment_date)						
select r.customer_id, r.staff_id, r.rental_id, 
		(select rental_rate from film 
		where upper(title) ='THE LORD OF THE RINGS: THE RETURN OF THE KING'),
		current_timestamp 
from rental r
	 where r.customer_id in (select customer_id 
			from customer 
			where  upper(concat(first_name,' ', last_name))='IHAR KAMKO')
			and r.rental_id in 
				(select r.rental_id from rental r 
					inner join inventory i on r.inventory_id = i.inventory_id 
					inner join film f on i.film_id =f.film_id 
					and upper(f.title) = 'THE LORD OF THE RINGS: THE RETURN OF THE KING')
order by r.rental_id desc 																   
limit 1
returning *;

insert into payment (customer_id, staff_id, rental_id, amount, payment_date)
select r.customer_id, r.staff_id, r.rental_id, 
		(select rental_rate from film 
		where upper(title) ='THE GREEN MILE'),
		current_timestamp 
from rental r
	 where r.customer_id in (select customer_id 
			from customer 
			where  upper(concat(first_name,' ', last_name))='IHAR KAMKO')
			and r.rental_id in 
				(select r.rental_id from rental r 
					inner join inventory i on r.inventory_id = i.inventory_id 
					inner join film f on i.film_id =f.film_id 
					and upper(f.title) = 'THE GREEN MILE')
order by r.rental_id desc 																	--if I rent same film again could be created wrong rows with same rental_id. So we need get the last rental_id of same film_id   
limit 1
returning *;

insert into payment (customer_id, staff_id, rental_id, amount, payment_date)
select r.customer_id, r.staff_id, r.rental_id, 
		(select rental_rate from film 
		where upper(title) ='THE SHAWSHANK REDEMPTION'),
		current_timestamp 
from rental r
	 where r.customer_id in (select customer_id 
			from customer 
			where  upper(concat(first_name,' ', last_name))='IHAR KAMKO')
			and r.rental_id in 
				(select r.rental_id from rental r 
					inner join inventory i on r.inventory_id = i.inventory_id 
					inner join film f on i.film_id =f.film_id 
					and upper(f.title) = 'THE SHAWSHANK REDEMPTION')
order by r.rental_id desc 																	   
limit 1
returning *;
