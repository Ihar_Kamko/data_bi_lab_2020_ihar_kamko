--Prepare answers to the following questions:
--1)How can one restrict access to certain columns of a database table?
--We can restrict access by listing the names of columns
GRANT SELECT (column_name1, column_name2) on table_name to role_name

--2)What is the difference between user identification and user authentication?
--Authentication Is a procedure for verifying a user's rights to access information or perform certain actions. 
--Authentication cannot determine the identity of the user, but it can indicate exactly what rights he has
--Identification it is an accurate identification of a user based on various characteristics. 
--Use of a password is a user identification based on one characteristic. Authentication allows the system to establish a username




--3)What are the recommended authorization protocols for PostgreSQL?
--'trust' is convenient for working at home on local DB for single-user PC;

--authorization protocol GSSAPI is recommended industry standard for secure authorization but needs to use a secure socket layer (SSL) to secure the sent data during the connection to the database;

--If database servers are isolated from the world using firewalls, we can use the SCRAM-SHA-256 authentication method and limit the IP addresses. 
--So that the database server accepts connections within a certain range or set; (was used at my former job)
--If the application server and database server are not on the same machine, we can use a strong authentication method, such as LDAP and Kerberos.

--I could list the entire list of different authentication protocols, but it seems to me that three are the most convenient when using at home and in industry, respectively

--4)What is proxy authentication in PostgreSQL and what is it for? 
--Proxy authentication is authentication method. 
--This method uses 1 role for all users, and multiple roles with differentiate privileges.
--When some user connects to the database with web application, for example, database invokes the SET ROLE command based on the user class.

--Why does it make the previously discussed role-based access control easier to implement?
--It simplifies the differentiation of privileges when users connecting to the database