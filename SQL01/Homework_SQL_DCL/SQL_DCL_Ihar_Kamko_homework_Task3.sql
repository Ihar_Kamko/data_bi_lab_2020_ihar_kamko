--Read about row-level security (https://www.postgresql.org/docs/12/ddl-rowsecurity.html) and configure it for your database, so that the
--customer can only access his own data in "rental" and "payment" tables (verify using the personalized role you previously created).
ALTER TABLE rental ENABLE ROW LEVEL SECURITY;
ALTER TABLE payment ENABLE ROW LEVEL SECURITY;

GRANT SELECT ON rental, payment, customer  TO "client_Patricia_Johnson";		--add privileges to 

CREATE POLICY customer_rental ON rental
for SELECT to "client_Patricia_Johnson"
using (customer_id in (select customer_id 
					   from customer 
					   where ('client'||'_'||lower(first_name)||'_'||lower(last_name) = lower(user)))); 

CREATE POLICY customer_payment ON payment
for SELECT to "client_Patricia_Johnson"
using (customer_id in (select customer_id 
					   from customer 
					   where ('client'||'_'||lower(first_name)||'_'||lower(last_name) = lower(user))));
					   
