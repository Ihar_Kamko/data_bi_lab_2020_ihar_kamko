--Implement role-based authentication model for dvd_rental database:
--Create group roles:
--DB developer
CREATE GROUP db_developer;
GRANT USAGE ON SCHEMA public, pg_catalog, information_schema TO db_developer;
GRANT ALL ON ALL TABLES IN SCHEMA public, pg_catalog, information_schema TO db_developer;
GRANT ALL PRIVILEGES ON DATABASE postgres TO db_developer;

--backend tester (read-only)
CREATE GROUP backend_tester;
GRANT SELECT ON ALL TABLES IN schema public to backend_tester;
REVOKE CREATE ON DATABASE postgres from backend_tester; 				--as it schema 'public' all users allow to create tables in it

--customer (read-only for film and actor)
CREATE GROUP customers;
GRANT SELECT ON actor, film  TO customers;
REVOKE CREATE ON DATABASE postgres from customers;

--Create personalized role for any customer already existing in the dvd_rental database. Role name must be client_{first_name}_{last_name}(omit curly brackets). 
--Customer's payment and rental history must not be empty.
CREATE USER "client_Patricia_Johnson"  PASSWORD '1';
GRANT SELECT ON rental, payment, customer  TO "client_Patricia_Johnson";

--Assign proper privileges to each role.
ALTER DEFAULT PRIVILEGES IN SCHEMA public, pg_catalog, information_schema GRANT ALL ON TABLES TO db_developer;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO backend_tester;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO customers;

--Verify that all roles are working as intended.
CREATE USER pavlovich IN GROUP db_developer PASSWORD '1';	--allow all privileges
CREATE USER ihar IN GROUP backend_tester PASSWORD '1'; 		--allow only SELECT
CREATE USER kamko IN GROUP customers PASSWORD '1'; 			--allow only select SELECT in tables actor, film 