--Figure out what security precautions are already used in your 'dvd_rental' database; -- send description

--All roles that have dvd_rental database are default.

--1)pg_execute_server_program
--Allow executing programs on the database server as the user the database runs as with COPY and other functions which allow executing a server-side program.

--2)pg_monitor
--Read/execute various monitoring views and functions. This role is a member of pg_read_all_settings, pg_read_all_stats and pg_stat_scan_tables.

--3)pg_read_all_settings
--Read all configuration variables, even those normally visible only to superusers.

--4)pg_read_all_stats
--Read all pg_stat_* views and use various statistics related extensions, even those normally visible only to superusers.

--5)pg_read_server_files
--Allow reading files from any location the database can access on the server with COPY and other file-access functions.

--6)pg_signal_backend
--Signal another backend to cancel a query or terminate its session.

--7)pg_stat_scan_tables
--Execute monitoring functions that may take ACCESS SHARE locks on tables, potentially for a long time.

--8)pg_write_server_files
--Allow writing to files in any location the database can access on the server with COPY and other file-access functions.

--9)postgres
--default user that having superuser rights.