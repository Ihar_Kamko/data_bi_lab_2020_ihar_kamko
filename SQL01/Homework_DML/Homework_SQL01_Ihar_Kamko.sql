--Part 1
--All comedy movies released between 2000 and 2004, alphabetical
SELECT f.title 
FROM film f 
	INNER JOIN film_category fc ON f.film_id=fc.film_id 			--join film and film_category
	INNER JOIN category c ON fc.category_id =c.category_id 			--join with category
WHERE c.name='Comedy' 												--select only Comedies
AND f.release_year BETWEEN 2000 AND 2004 							--in
ORDER BY f.title ASC												--sorting by title alphabetical


--Revenue of every rental store for year 2017 (columns: address and address2 � as one column, revenue)
SELECT _group_concat(a.address, a.address2) AS full_store_address, sum(p.amount) AS revenue   --cast types
FROM store s 
INNER JOIN address a ON s.address_id = a.address_id											  --join with address
INNER JOIN staff s2 ON s.store_id = s2.store_id 											  --join with staff
INNER JOIN payment p ON s2.staff_id = p.staff_id											  --join with payment
WHERE p.payment_date BETWEEN '2017-01-01' AND '2017-12-31'									  --select range 
GROUP BY full_store_address																	  --group by store address


--Top-3 actors by number of movies they took part in (columns: first_name, last_name, number_of_movies, sorted by number_of_movies in descending order)
SELECT a.first_name, a.last_name, count(*) AS number_of_movies	--selecting actors ID and count of films where they took part 
FROM actor a
INNER JOIN film_actor fa on a.actor_id =fa.actor_id 			--join with film_actor
GROUP BY a.first_name, a.last_name, a.actor_id 					--group by first name, last name and actor_id, cause 2 actors have similar first name, last name 
ORDER BY number_of_movies DESC									--sorting by number of movies
LIMIT 3															--leaving only top 3 actors


--Number of comedy, horror and action movies per year (columns: release_year, number_of_action_movies,number_of_horror_movies, number_of_comedy_movies), sorted by release year in descending order
SELECT f.release_year, 
sum(cast(c.name ='Action' AS int)) AS number_of_action_movies,							--casting true or false to int and sum it for action movies
sum(cast(c.name ='Horror' AS int)) AS number_of_horror_movies, 							--for horror movies
sum(cast(c.name ='Comedy' AS int)) AS number_of_comedy_movies 							--for comedy movies
FROM film_category fc
INNER JOIN film f ON fc.film_id =f.film_id 												--join with category's table
INNER JOIN category c ON fc.category_id = c.category_id 								--with action films
GROUP BY f.release_year 
ORDER BY f.release_year DESC


--Part 2
--Which staff members made the highest revenue for each store and deserve a bonus for 2017 year?
WITH staff_revenue AS 
	(SELECT s.staff_id, concat(first_name ||' '|| last_name) AS staff_full_name, sum(amount) AS revenue, s.store_id  --selecting full name of staff, revenue, id of store
	FROM staff s
	INNER JOIN payment p on s.staff_id =p.staff_id 																	 --join with payment
	GROUP BY s.staff_id,s.store_id)																					 --group with staff id and store id
SELECT 	staff_full_name, revenue 																					 --selecting name and revenue from cte
FROM staff_revenue 
WHERE revenue IN 																									 
(SELECT max(revenue)																								 --selecting max revenue for every store
FROM staff_revenue
GROUP BY store_id)

--Which 5 movies were rented more than others and what's expected audience age for those movies?
WITH abc AS 
(SELECT title, rating::text, count(f.film_id) as rented_times								--cast type and counting films
FROM inventory i 
INNER JOIN film f ON i.film_id = f.film_id 													--join with film
INNER JOIN  rental r ON i.inventory_id = r.inventory_id 									--join with inventory
GROUP BY title, rating)																		--group by title and rating of films 
SELECT title,																				--selecting film's names																		
	(CASE 																					--cast rating names to  expected audience age 
		WHEN rating='PG' THEN '6+'
		WHEN rating='PG-13' THEN '13+'
		WHEN rating='NC-17' THEN 'less than 18 only with adults'
		WHEN rating='R' THEN '18+'
		WHEN rating='G' THEN '0+'
	end) as  expected_audience_age
FROM abc 
WHERE rented_times IN
(select rented_times FROM abc ORDER BY rented_times DESC LIMIT 5)							--selecting top 5 values of rented_times
--there is 7 films, cause 4 films have same values of rented_times

--Which actors/actresses didn't act for a longer period of time than others?
SELECT a.first_name, a.last_name, max(f.release_year) AS last_film 					--selecting actors and last films they took part
FROM film_actor fa 
INNER JOIN film f ON fa.film_id =f.film_id 											--join with film
INNER JOIN actor a ON fa.actor_id = a.actor_id 										--join with actor
GROUP BY fa.actor_id, a.first_name, a.last_name										--group by actor_id and actors first name and last name
ORDER BY max(f.release_year)  ASC													--sorting by last film
LIMIT 1																				--leaving actor who haven't played in films for a long time


