--Top-3 most selling movie categories of all time and total dvd rental income for each category. Only consider dvd rental customers from the USA.
select c.name as category_name, sum(p.amount) as total_rental_income
from inventory i 
	inner join film f on i.film_id=f.film_id 
	inner join film_category fc on f.film_id =fc.film_id 
	inner join category c on fc.category_id =c.category_id 
	inner join rental r on i.inventory_id =r.rental_id 
	inner join payment p on r.rental_id = p.rental_id 
	inner join customer c2 on r.customer_id = c2.customer_id
	and c2.customer_id in 
		(select customer_id from customer c2
			inner join address a2 on c2.address_id = a2.address_id
			inner join city on a2.city_id = city.city_id 
			and city.country_id in 
				(select country_id
					from country c 
						where upper(c.country) ='UNITED STATES'))
group by category_name
order by total_rental_income desc
limit 3;


--For each client, display a list of horrors that he had ever rented (in one column, separated by commas), and the amount of money that he paid for it

with customer_horrors as (select c2.customer_id, c2.first_name, c2.last_name, f.title, sum(p.amount) as payment
from inventory i 
	inner join film f on i.film_id=f.film_id 
	inner join film_category fc on f.film_id =fc.film_id 
	inner join category c on fc.category_id =c.category_id 
	and upper(c.name) ='HORROR'
	inner join rental r on i.inventory_id =r.rental_id 
	inner join payment p on r.rental_id = p.rental_id 
	inner join customer c2 on r.customer_id = c2.customer_id
group by c2.customer_id, c2.first_name, c2.last_name, f.title)

select c.customer_id, c.first_name, c.last_name, group_concat(c.title) as list_of_horrors, sum(c.payment) as total_payment
from customer_horrors c
group by c.customer_id, c.first_name, c.last_name