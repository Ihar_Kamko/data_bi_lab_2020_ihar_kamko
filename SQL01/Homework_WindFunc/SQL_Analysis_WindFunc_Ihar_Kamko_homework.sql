--TASK 1
--Build the query to generate a report about the most significant customers (which have maximum sales) through various sales channels.
--The 5 largest customers are required for each channel.
--Column sales_percentage shows percentage of customerís sales within channel sales
SELECT channel_desc, cust_last_name, cust_first_name, to_char(amount_sold, '9999999.99') AS amount_sold, to_char(round(sales_percentage, 5), ' 9.99999 %') AS sales_percentage 
FROM (
SELECT  c.channel_desc, 
		c2.cust_last_name, 
		c2.cust_first_name,
		c2.cust_year_of_birth,
		sum(amount_sold) AS amount_sold,
		sum(amount_sold)*100/sum(sum(amount_sold)) OVER (PARTITION BY c.channel_desc) AS sales_percentage,
		rank() OVER (PARTITION BY c.channel_desc ORDER BY sum(amount_sold) DESC) AS rating
FROM sales s 
	INNER JOIN channels c ON s.channel_id =c.channel_id 
	INNER JOIN customers c2 ON s.cust_id =c2.cust_id
GROUP BY c.channel_desc, c2.cust_last_name, c2.cust_first_name, c2.cust_id
ORDER BY rating ASC) abc
WHERE rating <=5
ORDER BY channel_desc ASC, amount_sold DESC, cust_year_of_birth ASC

--TASK 2
--Compose query to retrieve data for report with sales totals for all products in Photo category in Asia (use data for 2000 year). Calculate report total (YEAR_SUM).
CREATE EXTENSION IF NOT EXISTS tablefunc;

SELECT *,
	   to_char(coalesce(q1::float,0)+coalesce(q2::float,0)+coalesce(q3::float,0)+coalesce(q4::float,0), '999999.99') AS year_sum
FROM crosstab(
$$
SELECT p.prod_name::text AS product_name, t.calendar_quarter_number, to_char(sum(s.amount_sold)::decimal(8,2), '999999.99') --, sum(sum(s.amount_sold)) over (partition by p.prod_name)
FROM sales s
	INNER JOIN products p ON s.prod_id =p.prod_id 
	INNER JOIN customers c ON s.cust_id =c.cust_id 
	INNER JOIN countries c2 ON c.country_id =c2.country_id
	INNER JOIN times t ON s.time_id =t.time_id 
WHERE c2.country_subregion ='Asia'
AND p.prod_category ='Photo'
AND EXTRACT(year FROM s.time_id) =2000
GROUP BY p.prod_name, t.calendar_quarter_number
ORDER BY p.prod_name
$$) AS abc(prod_name text, "q1" text, "q2" text, "q3" text, "q4" text)
GROUP BY abc.prod_name, q1,q2,q3,q4
ORDER BY prod_name

--TASK 3
--Build the query to generate a report about customers who were included into TOP 300 (based on the amount of sales) in 1998, 1999 and 2001. 
--This report should separate clients by sales channels, and, at the same time, channels should be calculated independently (i.e. only purchases made onselected channel are relevant).

WITH abc AS (
SELECT t.calendar_year, c.channel_desc, c2.cust_id, c2.cust_last_name, c2.cust_first_name, sum(amount_sold) AS total,
	rank() OVER (PARTITION BY (t.calendar_year, c.channel_desc) ORDER BY sum(amount_sold) DESC)
FROM sales s
	INNER JOIN channels c ON s.channel_id =c.channel_id 
	INNER JOIN customers c2 ON s.cust_id =c2.cust_id 
	INNER JOIN times t ON s.time_id =t.time_id
WHERE EXTRACT(YEAR FROM s.time_id) IN ('1998', '1999', '2001')
GROUP BY t.calendar_year, c.channel_desc, c2.cust_last_name, c2.cust_first_name, c2.cust_id 
ORDER BY total DESC)

SELECT abc.channel_desc, abc.cust_id, cust_last_name, cust_first_name, to_char(sum(total), '99999.99') as amount_sold
FROM abc
INNER JOIN 
		(SELECT channel_desc, cust_id
		FROM abc
		WHERE (rank<=300 AND calendar_year='1998')
		INTERSECT 
		SELECT channel_desc, cust_id
		FROM abc
		WHERE (rank<=300 AND calendar_year='1999')
		INTERSECT 
		SELECT channel_desc, cust_id
		FROM abc
		WHERE (rank<=300 AND calendar_year='2001')) AS bca ON abc.channel_desc = bca.channel_desc AND abc.cust_id=bca.cust_id
GROUP BY abc.channel_desc, abc.cust_id, cust_last_name, cust_first_name
ORDER BY amount_sold DESC

--TASK 4
--Build the query to generate the report about sales in America and Europe:
WITH abc AS (
	SELECT t.calendar_month_desc, to_char(round(sum(amount_sold),0), '9,999,999')  AS summary, p.prod_category, c2.country_region,
		rank() OVER (PARTITION BY p.prod_category ORDER BY  c2.country_region) AS ranked

	FROM sales s		
		INNER JOIN customers c ON s.cust_id =c.cust_id 
		INNER JOIN countries c2 ON c.country_id =c2.country_id
		INNER JOIN products p ON s.prod_id = p.prod_id
		INNER JOIN times t ON s.time_id =t.time_id 
	WHERE s.time_id IN (
		SELECT time_id 
		FROM times 
		WHERE calendar_month_desc BETWEEN '2000-01' AND '2000-03')
	AND (c2.country_region = 'Americas' or c2.country_region = 'Europe')
	GROUP BY t.calendar_month_desc, p.prod_category, c2.country_region
	ORDER BY t.calendar_month_desc, p.prod_category asc)

SELECT  a.calendar_month_desc, a.prod_category, a.summary AS "Americas SALES", b.summary as "Europe SALES"
FROM abc a
	INNER JOIN abc b ON a.calendar_month_desc=b.calendar_month_desc AND a.prod_category = b.prod_category AND a.ranked<>b.ranked
WHERE a.ranked IN (SELECT ranked 
				   FROM abc
				   WHERE country_region IN (SELECT min(country_region) FROM abc))
				   
				   
				   
