CREATE OR REPLACE FUNCTION sudoku_solving(input_data text)
RETURNS TABLE (row_numb int, digits text)
LANGUAGE plpgsql
AS $function$

DECLARE
	solved_data text;

BEGIN 
	IF length(input_data) < 81 THEN RAISE EXCEPTION 'Less than 81 digits';
	END IF;
	IF length(input_data) > 81 THEN RAISE EXCEPTION 'More than 81 digits';
    END IF;

WITH RECURSIVE sudoku(board, blank) AS
	(SELECT foo.input_data, position(' ' IN foo.input_data) /*first position without digit*/
  	FROM  (SELECT input_data) foo
  
	UNION ALL
  
	SELECT substring(board, 1, blank-1) || value || substring(board, blank +1), 
		   position(' ' in repeat('1',blank) || substring(board, blank+1)) --position offset
	FROM sudoku, (SELECT gen::text AS value FROM generate_series(1,9) AS gen) fill
	WHERE blank IS NOT NULL 
	AND NOT EXISTS 
		(SELECT NULL
        FROM generate_series(1,9) gen
        WHERE fill.value = substring(board, ((blank-1)/9)*9 + gen,1) 
        OR fill.value = substring(board, mod(blank-1, 9)-8 + gen*9, 1 )
        OR fill.value = substring(board, mod(((blank-1)/3),3)*3 + ((blank-1)/27)*27 + gen + ((gen-1)/3)* 6, 1)
        )
   	 )  
    
SELECT board INTO solved_data
FROM sudoku
WHERE blank =0;


--data output with recursive query from 81 symbols text array
RETURN query 
WITH RECURSIVE abra(row, digits, rest) AS 
	(
		SELECT 
			   1 AS row,
		       left(solved_data, 9) AS digits,
		       right(solved_data, length(solved_data) - 9) as rest

		UNION ALL 
	
		SELECT 
			   abra.row +1 AS row,
			   left(abra.rest, 9) AS digits,
			   right(abra.rest, length(abra.rest) -9) AS rest
		FROM abra
		WHERE abra.rest <>''
	)

SELECT abra.row, abra.digits
FROM abra;

END;
$function$
;

select * from sudoku_solving(' 37 4  92   3   5  48       12 5 6      132    67 93      81   9       42 3  4  6');

select * from sudoku_solving('87          9    4 2 7  1 5  96   3         9  654    69    7  2    74     3   1 ');