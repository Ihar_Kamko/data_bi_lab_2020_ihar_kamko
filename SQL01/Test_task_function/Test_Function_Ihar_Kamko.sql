/*Create one function that reports all information for a particular client and timeframe:
� Customer's name, surname and email address;
� Number of films rented during specified timeframe;
� Comma-separated list of rented films at the end of specified time period;
� Total number of payments made during specified time period;
� Total amount paid during specified time period;
Function's input arguments: client_id, left_boundary, right_boundary.
The function must analyze specified timeframe [left_boundary, right_boundary] and output specified information for this timeframe.
Function's result format: table with 2 columns �metric_name� and �metric_value�.*/
CREATE OR REPLACE FUNCTION client_info
		(client_id					int, 
		left_boundary 				date, 
		right_boundary 				date)
RETURNS TABLE (metric_name text, metric_value text)
LANGUAGE plpgsql
AS $function$
declare
begin 
	IF left_boundary >= current_date THEN
        RAISE EXCEPTION 'Wrong date';
    END IF;
    
    IF right_boundary >= current_date THEN
        RAISE EXCEPTION 'Wrong date';
    END IF;
   
    IF left_boundary >  right_boundary THEN
        RAISE EXCEPTION 'End date cannot be less than start date';
    END IF;

return QUERY 
with abc as (
  SELECT group_concat(distinct c.first_name||', '|| c.last_name ||', '|| c.email)::text as customer, 
  		 count(r.rental_id)::text as rented, 
  		 group_concat( f.title):: text as films, 
  		 count(p.payment_id)::text as payments, 
  		 sum(p.amount)::text as amount
	FROM rental r
	inner join payment p on r.rental_id = p.rental_id 
	inner join customer c on p.customer_id =c.customer_id 
	inner join inventory i on r.inventory_id =i.inventory_id 
	inner join film f on i.film_id =f.film_id 
	where p.payment_date between left_boundary  and right_boundary
	and r.customer_id = client_id)

	select 'customer''s info', customer  from abc
	union all
	select 'num. of films rented', rented from abc
	union all
	select 'rented films', films from abc
	union all
	select 'num. of payments', payments from abc
	union all
	select 'payments'' amount', amount from abc;
end
$function$
;

--don't get why i caught an exeption when try to return table... 

select 	client_info(1, '2017-03-01','2017-12-05')
