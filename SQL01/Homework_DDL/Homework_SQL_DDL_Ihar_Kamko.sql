--1)Create all tables for the relational model you created while studying DB Basics module (fixed model in 3nf). 
--Create a separate database for them and give it appropriate domain-related name. Make sure you choose optimal datatype for each column. 
--Use NOT null constraints, default values and generated columns where appropriate.

CREATE SCHEMA new_musical_store;					--creating new schema 

SET search_path TO new_musical_store, public;		--changing searh pass. So we able not to indicate our schema every time, when we refer to it


--as I think 1st of all we need to create table that have no references

CREATE EXTENSION IF NOT EXISTS citext; 				--creating extension of case insensitive text for unique constraint

CREATE TABLE instruments
(
	instrument_id 		smallserial PRIMARY KEY,
	instrument_name 	citext NOT NULL UNIQUE		--the example of unique constraint citext was created for
);

CREATE TABLE musicians_roles 
(
	musician_role_id 	smallserial PRIMARY KEY,
	musician_role 		citext NOT NULL UNIQUE
);

CREATE TABLE nationalities
(
	nationality_id 		smallserial PRIMARY KEY,
	nationality 		citext NOT NULL UNIQUE
);

CREATE TABLE musicians 
(
	musician_id 		smallserial PRIMARY KEY,
	first_name 			citext NOT NULL,
	last_name 			citext NOT NULL,
 	date_of_birth 		date NOT NULL,
	nationality_id 		smallint NOT null REFERENCES nationalities ON DELETE RESTRICT ON UPDATE CASCADE,
	role_id 			smallint NOT null REFERENCES musicians_roles ON DELETE RESTRICT ON UPDATE cascade,
	CONSTRAINT musicians_unique UNIQUE(first_name, last_name, date_of_birth, nationality_id)				--prevents inserting same musician
);

CREATE TABLE musicians_instuments 
(
	musician_id 		smallint NOT NULL REFERENCES musicians ON DELETE RESTRICT ON UPDATE CASCADE,
	instrument_id 		smallint NOT NULL REFERENCES instruments ON DELETE RESTRICT ON UPDATE cascade,
	PRIMARY KEY			(musician_id,instrument_id )
);

CREATE TABLE ensembles_types 
(
	ensemble_type_id 	smallserial PRIMARY KEY,
	ensemble_type  		citext NOT NULL unique
);

CREATE TABLE ensembles 
(
	ensemble_id 		smallserial PRIMARY KEY,
	ensemble_name 		citext NOT NULL,
	style 				text NOT NULL,
	formed 				smallint NOT NULL,
	description 		varchar(300) DEFAULT NULL,
	type_id 			smallint NOT NULL REFERENCES ensembles_types ON DELETE RESTRICT ON UPDATE CASCADE,
	CONSTRAINT ensembles_adress UNIQUE(ensemble_name, formed)					--prevents insert same ensemble
);

CREATE TABLE ensembles_musicians 
(
	ensemble_id 		smallint NOT NULL REFERENCES ensembles ON DELETE RESTRICT ON UPDATE CASCADE,
	musician_id 		smallint NOT NULL REFERENCES musicians ON DELETE RESTRICT ON UPDATE CASCADE,
	PRIMARY key			(ensemble_id, musician_id)
);
	
CREATE TABLE compositions 
(
	composition_id 		serial PRIMARY KEY,
	composition_name 	citext NOT NULL UNIQUE,
	description			text DEFAULT NULL,
	ensemble_id			smallint NOT NULL REFERENCES ensembles ON DELETE RESTRICT ON UPDATE cascade
);

CREATE TABLE suppliers 
(
	supplier_id			smallserial PRIMARY KEY,
	supplier_name 		citext NOT NULL,
	country 			citext NOT NULL,
	city 				citext NOT NULL,
	street 				citext NOT NULL,
	house_number		citext NOT NULL,
	post_code 			text NOT NULL,
	constraint supplier_adress UNIQUE(supplier_name, country, city, street, house_number)		--if our supplier have more than 2 outlets
);

CREATE TABLE vinyls 
(
	vinyl_id 			serial PRIMARY KEY,
	vinyl_number 		int NOT NULL UNIQUE,
	vinyl_name 			text NOT NULL,
	date_of_realese 	date NOT null default current_date,
	label 				text NOT NULL,
	store_price 		decimal(6,2)  NOT NULL,
	available_count 	smallint  NOT null default 0
);

CREATE TABLE compositions_vinyls
(
	vinyl_id 			smallint NOT NULL  PRIMARY KEY REFERENCES vinyls ON DELETE RESTRICT ON UPDATE CASCADE,
	track_id 			smallint NOT NULL,
	composition_id 		smallint NOT NULL REFERENCES compositions ON DELETE RESTRICT ON UPDATE CASCADE,
	UNIQUE 				(vinyl_id,track_id)
);

CREATE TABLE sales 
(
	sale_id 			serial PRIMARY KEY,
	sale_date 			timestamp  NOT null default current_timestamp,
	total_price 		decimal(6,2) NOT null
);


CREATE TABLE vinyls_sales 
(
	vinyl_id 			int NOT NULL REFERENCES vinyls ON DELETE RESTRICT ON UPDATE CASCADE,
	sale_id 			int NOT NULL REFERENCES sales ON DELETE RESTRICT ON UPDATE CASCADE,
	count 				smallint  NOT NULL,
	sold_at_price 		decimal(6,2)  NOT NULL,
  	PRIMARY KEY 		(vinyl_id, sale_id)
);

CREATE TABLE vinyls_suppliers 
(
	supplier_id 		smallint  NOT NULL REFERENCES suppliers ON DELETE RESTRICT ON UPDATE CASCADE,
	vinyl_id 			smallint  NOT NULL REFERENCES vinyls ON DELETE RESTRICT ON UPDATE CASCADE,
	retail_price 		decimal(6,2) DEFAULT NULL,
	trade_price 		decimal(6,2) DEFAULT NULL,
	PRIMARY KEY 		(supplier_id, vinyl_id),
	CHECK ((coalesce(retail_price, trade_price) is not null))
);

--in all references I set ON DELETE RESTRICT to protect data from accidental delete


--2)Create all table relationships with primary and foreign keys.
--it was created with table creatinon

--3) Create at least 5 check constraints, not considering unique and not null, on your tables (in total 5, not for each table)
--constraint that prevents insert ensembles with wrong formed year
ALTER TABLE ensembles ADD CONSTRAINT formed_year CHECK (formed<=EXTRACT(YEAR FROM current_date));
--google said that the younger singer in history was Michael Jackson (5 years old) so I set lower limit to 5 years(birthday 31.12.2014 for example now having difference in  5 years and 7 months)   
ALTER TABLE musicians ADD CONSTRAINT date_of_birth CHECK (extract(year from date_of_birth)<extract(year from current_date)-6);
--PostgreSQL doesn't support unsigned datatype so I need check that price columns have value >0 
ALTER TABLE sales ADD CONSTRAINT positive_price CHECK (total_price >0);
ALTER TABLE vinyls_sales ADD CONSTRAINT positive_price CHECK (sold_at_price >0);
ALTER TABLE vinyls ADD CONSTRAINT positive_price CHECK (store_price >0);
ALTER TABLE vinyls_suppliers ADD CONSTRAINT positive_trade_price CHECK (trade_price >0);
ALTER TABLE vinyls_suppliers ADD CONSTRAINT positive_retail_price CHECK (retail_price >0);
--restrict to insert vinyls that wasn't realese yet
ALTER TABLE vinyls ADD CONSTRAINT realese_date CHECK (date_of_realese<=current_date);
--we cannot have less than 0 count of vinyls in store
ALTER TABLE vinyls ADD CONSTRAINT count_check check (available_count>=0);

--4) Fill your tables with sample data (create it yourself, 20+ rows total in all tables, make sure each table has at least 2 rows)
INSERT INTO instruments 
VALUES (1, 'Guitar'), (2, 'Bass-guitar'), (3, 'Vocals'), (4, 'Drums');

INSERT INTO musicians_roles
VALUES (1, 'Frontman'), (2,'Drummer');

INSERT INTO nationalities
VALUES (1, 'Great Britain'), (2,'Germany');

INSERT INTO musicians
VALUES (1, 'Freddy', 'Mercury', '1946-09-05', 1, 1), (2,'Ringo', 'Starr', '1940-07-07', 1, 2);

INSERT INTO musicians_instuments 
VALUES (1,1), (1,3), (2,3), (2,4);

INSERT INTO ensembles_types
VALUES (1, '�������'), (2,'����');

INSERT INTO ensembles
VALUES (1, 'Queen', 'Rock', 1970, null, 1), (2, 'The Beatles', 'Rock-n-Roll', 1960, null, 1);

INSERT INTO ensembles_musicians 
VALUES (1,1), (2,2);

INSERT INTO compositions
VALUES (1, 'Show Must Go On', null, 1), (2, 'Yesterday', null, 2);

INSERT INTO suppliers
VALUES (1, 'Jhon''s Recording Company', 'Great Britain', 'Manchester', 'Warwick', '12', 'M1 1AH'), (2, 'Steven''s Recording Company', 'Great Britain', 'Liverpool', 'Simpson', '33', 'L1 0AX');
 
INSERT INTO vinyls 
VALUES (1, 55479121, 'Greatest Hits', '1981-10-05', 'Parlophone', 19.99, 11), (2, 24123874, 'Sgt. Pepper''s Lonely Hearts Club Band', '1967-10-05', 'Parlophone', 14.99, 8);  --they really was released by same label

INSERT INTO compositions_vinyls 
VALUES (1, 1, 1), (2, 1, 2);

INSERT INTO sales 
VALUES (1, current_timestamp, 19.99), (2, current_timestamp, 14.99);

INSERT INTO  vinyls_sales 
VALUES (1, 1, 1, 19.99), (2, 2, 1, 14.99);

INSERT INTO  vinyls_suppliers
VALUES (1, 1, null, 15.00), (1, 2, 13.00, 12.00), (2, 2, 14.00, null);


--5) Alter all tables and add 'record_ts' field to each table. Make it not null and set its default value to current_date. Check that the value has been set for existing rows.
ALTER TABLE instruments ADD COLUMN record_ts date NOT NULL DEFAULT current_date;
ALTER TABLE musicians_roles ADD COLUMN record_ts date NOT NULL DEFAULT current_date;
ALTER TABLE nationalities ADD COLUMN record_ts date NOT NULL DEFAULT current_date;
ALTER TABLE musicians ADD COLUMN record_ts date NOT NULL DEFAULT current_date;
ALTER TABLE musicians_instuments ADD COLUMN record_ts date NOT NULL DEFAULT current_date;
ALTER TABLE ensembles_types ADD COLUMN record_ts date NOT NULL DEFAULT current_date;
ALTER TABLE ensembles ADD COLUMN record_ts date NOT NULL DEFAULT current_date;
ALTER TABLE ensembles_musicians ADD COLUMN record_ts date NOT NULL DEFAULT current_date;
ALTER TABLE compositions ADD COLUMN record_ts date NOT NULL DEFAULT current_date;
ALTER TABLE suppliers ADD COLUMN record_ts date NOT NULL DEFAULT current_date;
ALTER TABLE vinyls ADD COLUMN record_ts date NOT NULL DEFAULT current_date;
ALTER TABLE compositions_vinyls ADD COLUMN record_ts date NOT NULL DEFAULT current_date;
ALTER TABLE sales ADD COLUMN record_ts date NOT NULL DEFAULT current_date;
ALTER TABLE vinyls_sales ADD COLUMN record_ts date NOT NULL DEFAULT current_date;
ALTER TABLE vinyls_suppliers ADD COLUMN record_ts date NOT NULL DEFAULT current_date;
--as we can see value'2020-08-04' is today. PostgreSQL DB do not keep data about time of insert rows in tables. We need to use column like this to keep that data.