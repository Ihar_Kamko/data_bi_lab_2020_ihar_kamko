--���������, �������� ����� ������ ������ ������ ����� (������� ������ 10 �����, ��������������� �� �������� ���������� ��������).
select c.country_name as country,  count(f.flight_id) as count_of_flights
from passengers p
inner join countries c on p.passenger_country =c.country_id 
inner join tickets t on p.passenger_id =t.passenger_id 
inner join flights_tickets ft on t.ticket_id =ft.ticket_id 
inner join flights f on ft.flight_id=f.flight_id
group by country
order by count_of_flights desc 
limit 10;

--������������ ����� ����� ��������� ����� ������� ���������� �������� (������� 5 �����, ������������� �� �������� ���������� ��������).
select c.country_name as country,  count(f.flight_id) as count_of_flights
from flights f
inner join airlines a on f.airline_id =a.airline_id 
inner join countries c on a.airline_country =c.country_id 
group by c.country_name 
order by count_of_flights desc 
limit 5;

--����� ������ ������: ������������ ��� ��������?
select 
	case
	   when count(*)-count(ft.flight_id)<count(*)/2 then 'More passenger flights'
	   when count(*)-count(ft.flight_id)=count(*)/2 then 'Same number of passenger and cargo flights'
	   else 'More cargo flights'
	end
from flights f 
left join (select distinct flight_id from flights_tickets ft) ft on f.flight_id =ft.flight_id;

--���������� ���������� ����������� �������� � ������������ ��������� ��� ��������� ������������.
select a.airline_name,
	    round((count(*)-count(ft.flight_id))::decimal(4,2)*100/count(*),2)  as percent_of_cargo_flights,
	    round(count(ft.flight_id)::decimal(4,2)*100/count(*),2) as percent_of_passenger_flights
from flights f 
left join (select distinct flight_id from flights_tickets ft) ft on f.flight_id =ft.flight_id
inner join airlines a on f.airline_id =a.airline_id 
group by airline_name 
order by airline_name

--����� �����, ������� �������� ����� ������������ (������� ������ 20 �� ��������� ���������� ��������, ���������� ����������� � ��������� ��������� ���� ������� ����� ���������).
select p.passenger_first_name||' ' ||p.passenger_last_name as full_name,
	   count(f.flight_id) as count_of_flights,
	   sum(f.flight_distance) as totally_distance_flied,
	   sum(t.ticket_price) as totally_paid
from passengers p
inner join tickets t on p.passenger_id=t.passenger_id 
inner join flights_tickets ft on t.ticket_id =ft.ticket_id 
inner join flights f on ft.flight_id =f.flight_id 
group by full_name, p.passenger_country 
order by count_of_flights desc
limit 20;

--����� �����, ������� �������� ������ ����� ���������� � ��������� �������������. ��� ����� ����������� (���������������) ��� ����������?
with abc as (
	select a.airline_name, p.passenger_first_name ||' '|| p.passenger_last_name AS full_name, 
			sum(f.flight_distance) AS total_distance 
	from passengers p 
		inner join tickets t on p.passenger_id=t.passenger_id 
		inner join flights_tickets ft on t.ticket_id =ft.ticket_id 
		inner join flights f on ft.flight_id =f.flight_id
		inner join airlines a on f.airline_id =a.airline_id 
	group by a.airline_name, full_name, p.passenger_country ) 

	
select airline_name, full_name, max(total_distance)
from abc 
group by airline_name, full_name
having max(total_distance) in (select distinct max(total_distance) from abc  group by  airline_name )
order by airline_name asc, full_name 

--�������� ������ � ������ 15% ������ �� ���������� ��� ���� (��. ����) ���������� � ���� ��������
create or replace view  top_passenger as 
(with abc as (
	select a.airline_id, p.passenger_id, sum(f.flight_distance) AS total_distance 
	from passengers p 
		inner join tickets t on p.passenger_id=t.passenger_id 
		inner join flights_tickets ft on t.ticket_id =ft.ticket_id 
		inner join flights f on ft.flight_id =f.flight_id
		inner join airlines a on f.airline_id =a.airline_id 
	group by a.airline_id, p.passenger_id, p.passenger_country ) 
	
select airline_id, passenger_id
from abc 
group by airline_id, passenger_id
having max(total_distance) in (select distinct max(total_distance) from abc group by airline_id)
order by airline_id);

update tickets
set ticket_price = ticket_price - (ticket_price*0.15)::NUMERIC(6,2)
where ticket_id in (
select ticket_id
from tickets t
inner join airlines a on t.ticket_code like '%'||a.airline_icao_code||'%'
where (a.airline_id, passenger_id) in (select airline_id, passenger_id from top_passenger))

--������� �� ���� ������ ���������� � ����������, ������� ���� ���������������� ����� ��� �� 5 ������.
delete from flights_tickets ft
where ft.ticket_id in (
	select ft.ticket_id from flights_tickets ft
	inner join tickets t on ft.ticket_id =t.ticket_id 
	inner join passengers p on t.passenger_id =p.passenger_id 
	where p.passenger_id in (select t.passenger_id 
					from tickets t  
					group by t.passenger_id 
					having count(t.passenger_id)<5));

ALTER TABLE airflights.tickets DROP CONSTRAINT tickets_passenger_id_fkey;				
ALTER TABLE airflights.tickets ADD CONSTRAINT tickets_passenger_id_fkey FOREIGN KEY (passenger_id) REFERENCES airflights.passengers(passenger_id) ON UPDATE CASCADE ON DELETE CASCADE;		

delete from passengers cascade
where passenger_id in (
					select t.passenger_id 
					from tickets t  
					group by t.passenger_id 
					having count(t.passenger_id)<5);

--�������� ������������ ����, ����������� ������������� "Belavia Belarusian Airlines" �� �������� "Minsk International Airport" - "Warsaw, Poland" (�� ���� ���������������� 57 ����������).
CREATE OR REPLACE FUNCTION insert_flight (
			ins_flight_code 			text, 
			ins_airline_icao_code 		text, 
			ins_airport_from_code 		text, 
			ins_airport_to_code 		text, 
			ins_flight_distance 		int)
 RETURNS int 
 LANGUAGE plpgsql
AS $function$
DECLARE
	var_flight_id			int;
	var_airline_id			int;
	var_airport_from_id		int;
	var_airport_to_id		int;
BEGIN
		select a.airline_id into  var_airline_id	
		from airlines a 
		where a.airline_icao_code = ins_airline_icao_code; 
		
		select a.airport_id into  var_airport_from_id
		from airports a
		where a.airport_iata_code = ins_airport_from_code; 
	
		select a.airport_id into var_airport_to_id
		from airports a
		where a.airport_iata_code = ins_airport_to_code;
		
			INSERT INTO flights (flight_code, airline_id, airport_from , airport_to, flight_distance) 
			VALUES (ins_flight_code, var_airline_id, var_airport_from_id, var_airport_to_id, ins_flight_distance)
			RETURNING flight_id into var_flight_id; 
     
    RETURN var_flight_id;  
    END;
$function$
;

CREATE OR REPLACE FUNCTION generate_values_for_flight 
	(ins_airline_icao_code 		varchar(20) default null, 
	 ins_airport_from_code 		varchar(20) default null,
	 ins_airport_to_code 		varchar(20) default null, 
	 ins_flight_code			varchar(20) default null,
	 ins_passenger_number 		int default 57, 
	 ins_econom_class_price 	decimal(6,2) default 114, 
	 ins_business_class_price 	decimal(6,2) default 128,
	 ins_first_class_price 		decimal(6,2) default 142, 
	 ins_flight_distance 		int default 548)
RETURNS TABLE (flight_id int, passenger_id int, travel_class_id int, ticket_code varchar(20), ticket_price decimal(6,2), flight_code varchar(20))
LANGUAGE plpgsql
AS $function$

DECLARE
 	var_flight_id			int;
	var_airline_icao_code	text;
	var_airport_from_code 	text; 
	var_airport_to_code 	text; 
	var_flight_code 		text;
	var_ticket_code 		text;
	var_travel_class_id		int;
	var_ticket_id			int;
	var_ticket_price 		decimal(6,2);

  BEGIN
	IF ins_airline_icao_code IS NOT NULL 
		THEN
			var_airline_icao_code=ins_airline_icao_code;
		ELSE
			SELECT a.airline_icao_code INTO var_airline_icao_code
			FROM airflights.airlines a 
			WHERE lower(a.airline_name) like '%belavia%';
	END IF; 

	IF ins_airport_from_code IS NOT NULL 
		THEN
			var_airport_from_code=var_airport_from_code;
		ELSE
			SELECT a.airport_iata_code INTO var_airport_from_code
			FROM airports a 
			WHERE lower(a.airport_name) = 'minsk international airport';
	END IF; 
	
	IF ins_airport_from_code IS NOT NULL 
		THEN
			var_airport_to_code=ins_airport_to_code;
		ELSE
			SELECT a.airport_iata_code INTO var_airport_to_code 
	  		FROM airports a 
		  	WHERE a.airport_location IN 
				(SELECT l.location_id 
				 FROM locations l 
				 WHERE lower(l.city) = 'warsaw' 
				 AND country_location= (SELECT country_id FROM countries 
					   WHERE lower(country_name) = 'poland'));
	END IF;
	
	IF ins_flight_code IS NOT NULL 
		THEN
			var_flight_code=ins_flight_code;
		ELSE
			SELECT a.airline_icao_code||(SELECT floor(random()*(999-100+1)+100)) into var_flight_code
			FROM airlines a
			WHERE lower(a.airline_name) like '%belavia%';
	END IF;

--�������� �����
	var_flight_id = insert_flight(var_flight_code, var_airline_icao_code, var_airport_from_code, var_airport_to_code, ins_flight_distance);

--c������� ������� ������� �� ������ ����	
	CREATE TEMPORARY TABLE tmp (ticket_id int, flight_id int, travel_class_id int, ticket_code varchar(20), ticket_price decimal(6,2), flight_code varchar(20));

	INSERT INTO tmp (ticket_id, flight_id, travel_class_id, flight_code)
	SELECT *, var_flight_id, 
	floor(random()*((select max(t.travel_class_id) from travel_classes t) - (select min(t.travel_class_id) from travel_classes t) + 1) + (select min(t.travel_class_id) from travel_classes t)), 
	var_flight_code
	FROM generate_series(1, ins_passenger_number); 

--��������� �������� ticket_code � ����������� �� ���������� ������� �� ������ ����
	UPDATE tmp 
	SET ticket_code = CASE 
		WHEN char_length(tmp.ticket_id::text)=1 THEN tmp.flight_code||'/00'||tmp.ticket_id
		WHEN char_length(tmp.ticket_id::text)=2 THEN tmp.flight_code||'/0'||tmp.ticket_id
		ELSE tmp.flight_code||'/'||tmp.ticket_id
	END;
--���������� ���������� ������� ������
	UPDATE tmp
		SET ticket_id=ticket_id+(SELECT max(t.ticket_id) FROM tickets t);
	
--���������� ���� � ����������� �� ������ ������
	UPDATE tmp 
    	SET ticket_price = 
    		CASE
            	WHEN tmp.travel_class_id = 1 THEN ins_econom_class_price
            	WHEN tmp.travel_class_id = 2 THEN ins_business_class_price
            	WHEN tmp.travel_class_id = 3 THEN ins_first_class_price
        	END;
--������� ��������� ����������
    CREATE TEMPORARY TABLE tmp2 (passenger_id int);
    INSERT  INTO tmp2 
    SELECT p.passenger_id FROM passengers p ORDER BY random() LIMIT ins_passenger_number;
        
	INSERT INTO tickets (ticket_id, ticket_code, ticket_class, ticket_price) 
	SELECT tmp.ticket_id, tmp.ticket_code, tmp.travel_class_id, tmp.ticket_price
	from tmp;
	
	INSERT INTO flights_tickets(flight_id, ticket_id)
	SELECT tmp.flight_id, tmp.ticket_id 
	FROM tmp;
    DROP TABLE tmp2;
	DROP TABLE tmp;
END;
$function$
;
select * from generate_values_for_flight();
--�� ���� ������ - ������� �������� ������� ���� � ������������ ������... � �� ����� ������ ���������� � ����������� �� �������.
--������ ������� passenger_id � ������� tickets ����� ���� NULL
