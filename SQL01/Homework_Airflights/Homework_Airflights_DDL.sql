/*create database airflights;*/
create schema airflights;
SET search_path TO airflights, public;

create table data 
	(AIRLINE_ICAO_CODE		varchar(100),
	AIRLINE_NAME			varchar(100),
	AIRLINE_CALL_SIGN		varchar(100),
	AIRLINE_COUNTRY			varchar(100),
	FLIGHT_CODE				char(6),
	AIRPORT_FROM_IATA_CODE	char(3),
	AIRPORT_FROM_NAME		varchar(100),
	AIRPORT_FROM_LOCATION	varchar(100),
	AIRPORT_TO_IATA_CODE	char(3),
	AIRPORT_TO_NAME			varchar(100),
	AIRPORT_TO_LOCATION		varchar(100),
	FLIGHT_DISTANCE			int,	
	PILOT_NAME				varchar(100),
	PILOT_SURNAME			varchar(100),
	COPILOT_NAME			varchar(100),
	COPILOT_SURNAME			varchar(100),
	PASSENGER_FIRST_NAME	varchar(100),
	PASSENGER_LAST_NAME		varchar(100),
	PASSENGER_COUNTRY		varchar(100),
	TRAVEL_CLASS			varchar(100),
	TICKET_CODE				char(10),
	TICKET_PRICE			decimal(6,2)
	);	

create table countries
	(country_id 	smallserial PRIMARY KEY,
	 country_name   varchar(50) UNIQUE NOT NULL);

	
create table airlines
	(airline_id			smallserial PRIMARY KEY, 
	 airline_icao_code	char(3) UNIQUE NOT NULL,
	 airline_name   	text NOT NULL,
	 airline_call_sign	varchar(30) NOT NULL,
	 airline_country	smallint NOT NULL REFERENCES countries(country_id) ON UPDATE CASCADE ON DELETE RESTRICT);
CREATE INDEX idx_fk_airline_country ON airlines USING btree (airline_country);

create table pilots
	(pilot_id 		smallserial PRIMARY KEY,
	 pilot_name		varchar(30) NOT NULL,
	 pilot_surname	varchar(30) NOT NULL);
CREATE INDEX idx_fk_pilots_name ON pilots USING btree (pilot_name, pilot_surname);
	
create table passengers
	(passenger_id 			serial PRIMARY KEY,
	 passenger_first_name	varchar(30) NOT NULL,
	 passenger_last_name	varchar(30) NOT NULL,
	 passenger_country		smallint NOT NULL REFERENCES countries(country_id) ON UPDATE CASCADE ON DELETE RESTRICT);
CREATE INDEX idx_fk_passenger_country ON passengers USING btree (passenger_country);

create table locations
	 (location_id 			smallserial PRIMARY KEY,
	  city					varchar(50) not NULL,
	  district				varchar(50),
	  region				varchar(50),
	  country_location		smallint NOT NULL REFERENCES countries(country_id) ON UPDATE CASCADE ON DELETE RESTRICT);	 
CREATE INDEX idx_fk_location_country ON locations USING btree (country_location);
	 
create table airports
	(airport_id 			serial PRIMARY KEY,
	 airport_iata_code		char(3) UNIQUE NOT NULL,
	 airport_name			varchar(50) NOT NULL,
	 airport_location		smallint NOT NULL REFERENCES locations(location_id) ON UPDATE CASCADE ON DELETE RESTRICT);
CREATE INDEX idx_fk_airport_location ON airports USING btree (airport_location);
	
create table travel_classes
	(travel_class_id 		serial PRIMARY KEY,
	 travel_class_name		varchar(30) UNIQUE NOT NULL);
	
create table flights
	(flight_id			serial PRIMARY KEY,
	 flight_code		varchar(20) UNIQUE NOT NULL,	
	 airline_id			smallint NOT NULL REFERENCES airlines(airline_id) ON UPDATE CASCADE ON DELETE RESTRICT,	
	 airport_from		int NOT NULL REFERENCES airports(airport_id) ON UPDATE CASCADE ON DELETE RESTRICT,
	 airport_to			int NOT NULL REFERENCES airports(airport_id) ON UPDATE CASCADE ON DELETE RESTRICT,
	 flight_distance	smallint NOT NULL);
CREATE INDEX idx_fk_airport_to ON flights USING btree (airport_to);
CREATE INDEX idx_fk_airport_from ON flights USING btree (airport_from);
CREATE INDEX idx_fk_airline ON flights USING btree (airline_id);

create table tickets
	(ticket_id			serial PRIMARY KEY,
	 ticket_code		varchar(20) UNIQUE NOT NULL,
	 ticket_class		smallint NOT NULL REFERENCES travel_classes(travel_class_id) ON UPDATE CASCADE ON DELETE RESTRICT,
	 ticket_price 		decimal(6,2) NOT NULL, 
	 passenger_id		int REFERENCES passengers(passenger_id) ON UPDATE CASCADE ON DELETE RESTRICT);
CREATE INDEX idx_fk_passenger_ticket ON tickets USING btree (passenger_id);
	
create table flights_tickets
	(flight_id			int NOT NULL REFERENCES flights(flight_id) ON UPDATE CASCADE ON DELETE RESTRICT,
	 ticket_id			int NOT NULL REFERENCES tickets(ticket_id) ON UPDATE CASCADE ON DELETE RESTRICT, 
	 primary key (flight_id, ticket_id) );
CREATE INDEX idx_fk_fl_tickets ON flights_tickets USING btree (ticket_id);
CREATE INDEX idx_fk_tic_flights ON flights_tickets USING btree (flight_id);

create table flights_crews
	(flight_id		int NOT NULL REFERENCES flights(flight_id) ON UPDATE CASCADE ON DELETE RESTRICT,
	 first_pilot	smallint NOT NULL REFERENCES pilots(pilot_id) ON UPDATE CASCADE ON DELETE RESTRICT,
	 second_pilot	smallint NOT NULL REFERENCES pilots(pilot_id) ON UPDATE CASCADE ON DELETE RESTRICT,
	 PRIMARY KEY (flight_id));
CREATE INDEX idx_fk_first_pilot ON flights_crews USING btree (first_pilot);
CREATE INDEX idx_fk_second_pilot ON flights_crews USING btree (second_pilot);
CREATE INDEX idx_fk_flight_id ON flights_crews USING btree (flight_id);


--deleted redundant indexes on PK
--added index for (pilot_name, pilot_surname)
--altered PK for table flights_crews - PK is flight, so we can add only one crew to one flight.

--Think about redesign some tables
--1) delete table flights_tickets and altering table tickets with add column flight_id references on flights.
--2) addition new table distance_between_airpots with (airport_from, airport_to, flight_distance);
