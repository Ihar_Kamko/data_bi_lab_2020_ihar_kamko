-- airflights.flights_crews definition

-- Drop table

-- DROP TABLE airflights.flights_crews;

CREATE TABLE airflights.flights_crews (
	flight_id int4 NOT NULL,
	first_pilot int2 NOT NULL,
	second_pilot int2 NOT NULL,
	CONSTRAINT flights_crews_pkey PRIMARY KEY (flight_id)
);
CREATE INDEX idx_fk_first_pilot ON airflights.flights_crews USING btree (first_pilot);
CREATE INDEX idx_fk_second_pilot ON airflights.flights_crews USING btree (second_pilot);


-- airflights.flights_crews foreign keys

ALTER TABLE airflights.flights_crews ADD CONSTRAINT flights_crews_first_pilot_fkey FOREIGN KEY (first_pilot) REFERENCES airflights.pilots(pilot_id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE airflights.flights_crews ADD CONSTRAINT flights_crews_flight_id_fkey FOREIGN KEY (flight_id) REFERENCES airflights.flights(flight_id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE airflights.flights_crews ADD CONSTRAINT flights_crews_second_pilot_fkey FOREIGN KEY (second_pilot) REFERENCES airflights.pilots(pilot_id) ON UPDATE CASCADE ON DELETE RESTRICT;