-- airflights.airlines definition

-- Drop table

-- DROP TABLE airflights.airlines;

CREATE TABLE airflights.airlines (
	airline_id smallserial NOT NULL,
	airline_icao_code bpchar(3) NOT NULL,
	airline_name text NOT NULL,
	airline_call_sign varchar(30) NOT NULL,
	airline_country int2 NOT NULL,
	CONSTRAINT airlines_airline_icao_code_key UNIQUE (airline_icao_code),
	CONSTRAINT airlines_pkey PRIMARY KEY (airline_id)
);
CREATE INDEX idx_fk_airline_country ON airflights.airlines USING btree (airline_country);


-- airflights.airlines foreign keys

ALTER TABLE airflights.airlines ADD CONSTRAINT airlines_airline_country_fkey FOREIGN KEY (airline_country) REFERENCES airflights.countries(country_id) ON UPDATE CASCADE ON DELETE RESTRICT;