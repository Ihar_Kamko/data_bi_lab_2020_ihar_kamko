-- airflights.pilots definition

-- Drop table

-- DROP TABLE airflights.pilots;

CREATE TABLE airflights.pilots (
	pilot_id smallserial NOT NULL,
	pilot_name varchar(30) NOT NULL,
	pilot_surname varchar(30) NOT NULL,
	CONSTRAINT pilots_pkey PRIMARY KEY (pilot_id)
);
CREATE INDEX idx_fk_pilots_name ON airflights.pilots USING btree (pilot_name, pilot_surname);