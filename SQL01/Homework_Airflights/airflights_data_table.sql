-- airflights."data" definition

-- Drop table

-- DROP TABLE airflights."data";

CREATE TABLE airflights."data" (
	airline_icao_code varchar(100) NULL,
	airline_name varchar(100) NULL,
	airline_call_sign varchar(100) NULL,
	airline_country varchar(100) NULL,
	flight_code bpchar(6) NULL,
	airport_from_iata_code bpchar(3) NULL,
	airport_from_name varchar(100) NULL,
	airport_from_location varchar(100) NULL,
	airport_to_iata_code bpchar(3) NULL,
	airport_to_name varchar(100) NULL,
	airport_to_location varchar(100) NULL,
	flight_distance int4 NULL,
	pilot_name varchar(100) NULL,
	pilot_surname varchar(100) NULL,
	copilot_name varchar(100) NULL,
	copilot_surname varchar(100) NULL,
	passenger_first_name varchar(100) NULL,
	passenger_last_name varchar(100) NULL,
	passenger_country varchar(100) NULL,
	travel_class varchar(100) NULL,
	ticket_code bpchar(10) NULL,
	ticket_price numeric(6,2) NULL
);