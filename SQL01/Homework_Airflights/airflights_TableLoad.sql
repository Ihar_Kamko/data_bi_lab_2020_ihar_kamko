insert into countries (country_name)
select distinct airline_country 
from data
union 
select distinct passenger_country 
from data
where passenger_country is not null
union
(with foo as ( 
select distinct regexp_split_to_array (airport_from_location ,',') as new
from data
union 
select distinct regexp_split_to_array (airport_to_location ,',') as new
from data)
select trim(new[2]) 
from foo
where array_length(new, 1)=2
union 
select trim(new[3])
from foo
where array_length(new, 1)=3
union 
select trim(new[4])
from foo
where array_length(new, 1)=4);

insert into airlines (airline_icao_code, airline_name, airline_call_sign, airline_country)
select distinct airline_icao_code, airline_name, airline_call_sign, c.country_id
from data
inner join countries c on data.airline_country= c.country_name;

insert into pilots (pilot_name, pilot_surname)
select distinct pilot_name, pilot_surname 
from data
union
select distinct copilot_name, copilot_surname 
from data;

insert into passengers (passenger_first_name, passenger_last_name, passenger_country)
select distinct  passenger_first_name, passenger_last_name, c.country_id 
from data
inner join countries c on data.passenger_country = c.country_name;

with foo as ( 
select distinct regexp_split_to_array (airport_from_location ,',') as new
from data
union 
select distinct regexp_split_to_array (airport_to_location ,',') as new
from data)
insert into locations (city, district, region, country_location)
select trim(new[1]), null, null, c.country_id 
from foo
inner join countries c on trim(new[2]) = c.country_name 
where array_length(new, 1)=2 
union 
select trim(new[1]), trim(new[2]), null, c.country_id 
from foo
inner join countries c on trim(new[3]) = c.country_name 
where array_length(new, 1)=3
union 
select trim(new[1]), trim(new[2]), trim(new[3]), c.country_id 
from foo
inner join countries c on trim(new[4]) = c.country_name 
where array_length(new, 1)=4;

with foo as ( 
select distinct airport_from_iata_code as iata_code, airport_from_name as name, regexp_split_to_array (airport_from_location ,',') as new
from data
union 
select distinct airport_to_iata_code, airport_to_name, regexp_split_to_array (airport_to_location ,',') as new
from data)
insert into airports (airport_iata_code, airport_name, airport_location)
select iata_code, name, l.location_id 
from foo
inner join countries c on trim(new[2]) = c.country_name 
inner join locations l on trim(new[1])=l.city and l.country_location =c.country_id 
where array_length(new, 1)=2 
union 
select iata_code, name, l.location_id 
from foo
inner join countries c on trim(new[3]) = c.country_name 
inner join locations l on trim(new[1])=l.city and trim(new[2])=l.district and l.country_location =c.country_id 
where array_length(new, 1)=3
union 
select iata_code, name, l.location_id 
from foo
inner join countries c on trim(new[4]) = c.country_name 
inner join locations l on trim(new[1])=l.city and trim(new[2])=l.district and trim(new[3])=l.region and l.country_location =c.country_id 
where array_length(new, 1)=4;

insert into travel_classes (travel_class_name)
select distinct travel_class 
from data
where travel_class is not null;

insert into flights (flight_code, airline_id, airport_from, airport_to, flight_distance )
select distinct flight_code , a.airline_id , a2.airport_id, a3.airport_id, flight_distance 
from data d
inner join airlines a on d.airline_icao_code = a.airline_icao_code
inner join airports a2 on d.airport_from_name =a2.airport_name
inner join airports a3 on d.airport_to_name =a3.airport_name;

insert into tickets (ticket_code, ticket_class, ticket_price, passenger_id)
select distinct ticket_code, travel_class_id, ticket_price, passenger_id
from data d
inner join travel_classes tc on d.travel_class =tc.travel_class_name
inner join passengers p on d.passenger_first_name = p.passenger_first_name and d.passenger_last_name =p.passenger_last_name
inner join countries c on p.passenger_country =c.country_id and d.passenger_country =c.country_name 
where ticket_code is not null;

insert into flights_tickets 
select distinct f.flight_id , t.ticket_id 
from data d
inner join flights  f on d.flight_code =f.flight_code
inner join tickets t on d.ticket_code =t.ticket_code 
where d.ticket_code is not null;

insert into flights_crews 
select distinct f.flight_id , p.pilot_id , p2.pilot_id 
from data d
inner join flights f on d.flight_code =f.flight_code
inner join pilots p on d.pilot_name =p.pilot_name and d.pilot_surname =p.pilot_surname 
inner join pilots p2 on d.copilot_name =p2.pilot_name and d.copilot_surname =p2.pilot_surname;