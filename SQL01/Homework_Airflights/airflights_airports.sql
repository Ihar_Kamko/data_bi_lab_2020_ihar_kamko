-- airflights.airports definition

-- Drop table

-- DROP TABLE airflights.airports;

CREATE TABLE airflights.airports (
	airport_id serial NOT NULL,
	airport_iata_code bpchar(3) NOT NULL,
	airport_name varchar(50) NOT NULL,
	airport_location int2 NOT NULL,
	CONSTRAINT airports_airport_iata_code_key UNIQUE (airport_iata_code),
	CONSTRAINT airports_pkey PRIMARY KEY (airport_id)
);
CREATE INDEX idx_fk_airport_location ON airflights.airports USING btree (airport_location);


-- airflights.airports foreign keys

ALTER TABLE airflights.airports ADD CONSTRAINT airports_airport_location_fkey FOREIGN KEY (airport_location) REFERENCES airflights.locations(location_id) ON UPDATE CASCADE ON DELETE RESTRICT;