-- airflights.flights definition

-- Drop table

-- DROP TABLE airflights.flights;

CREATE TABLE airflights.flights (
	flight_id serial NOT NULL,
	flight_code varchar(20) NOT NULL,
	airline_id int2 NOT NULL,
	airport_from int4 NOT NULL,
	airport_to int4 NOT NULL,
	flight_distance int2 NOT NULL,
	CONSTRAINT flights_flight_code_key UNIQUE (flight_code),
	CONSTRAINT flights_pkey PRIMARY KEY (flight_id)
);
CREATE INDEX idx_fk_airline ON airflights.flights USING btree (airline_id);
CREATE INDEX idx_fk_airport_from ON airflights.flights USING btree (airport_from);
CREATE INDEX idx_fk_airport_to ON airflights.flights USING btree (airport_to);


-- airflights.flights foreign keys

ALTER TABLE airflights.flights ADD CONSTRAINT flights_airline_id_fkey FOREIGN KEY (airline_id) REFERENCES airflights.airlines(airline_id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE airflights.flights ADD CONSTRAINT flights_airport_from_fkey FOREIGN KEY (airport_from) REFERENCES airflights.airports(airport_id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE airflights.flights ADD CONSTRAINT flights_airport_to_fkey FOREIGN KEY (airport_to) REFERENCES airflights.airports(airport_id) ON UPDATE CASCADE ON DELETE RESTRICT;