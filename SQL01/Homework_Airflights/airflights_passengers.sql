-- airflights.passengers definition

-- Drop table

-- DROP TABLE airflights.passengers;

CREATE TABLE airflights.passengers (
	passenger_id serial NOT NULL,
	passenger_first_name varchar(30) NOT NULL,
	passenger_last_name varchar(30) NOT NULL,
	passenger_country int2 NOT NULL,
	CONSTRAINT passengers_pkey PRIMARY KEY (passenger_id)
);
CREATE INDEX idx_fk_passenger_country ON airflights.passengers USING btree (passenger_country);


-- airflights.passengers foreign keys

ALTER TABLE airflights.passengers ADD CONSTRAINT passengers_passenger_country_fkey FOREIGN KEY (passenger_country) REFERENCES airflights.countries(country_id) ON UPDATE CASCADE ON DELETE RESTRICT;