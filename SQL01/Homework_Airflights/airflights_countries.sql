-- airflights.countries definition

-- Drop table

-- DROP TABLE airflights.countries;

CREATE TABLE airflights.countries (
	country_id smallserial NOT NULL,
	country_name varchar(50) NOT NULL,
	CONSTRAINT countries_country_name_key UNIQUE (country_name),
	CONSTRAINT countries_pkey PRIMARY KEY (country_id)
);