-- airflights.travel_classes definition

-- Drop table

-- DROP TABLE airflights.travel_classes;

CREATE TABLE airflights.travel_classes (
	travel_class_id serial NOT NULL,
	travel_class_name varchar(30) NOT NULL,
	CONSTRAINT travel_classes_pkey PRIMARY KEY (travel_class_id),
	CONSTRAINT travel_classes_travel_class_name_key UNIQUE (travel_class_name)
);