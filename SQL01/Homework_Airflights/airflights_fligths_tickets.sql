-- airflights.flights_tickets definition

-- Drop table

-- DROP TABLE airflights.flights_tickets;

CREATE TABLE airflights.flights_tickets (
	flight_id int4 NOT NULL,
	ticket_id int4 NOT NULL,
	CONSTRAINT flights_tickets_pkey PRIMARY KEY (flight_id, ticket_id)
);
CREATE INDEX idx_fk_fl_tickets ON airflights.flights_tickets USING btree (ticket_id);
CREATE INDEX idx_fk_tic_flights ON airflights.flights_tickets USING btree (flight_id);


-- airflights.flights_tickets foreign keys

ALTER TABLE airflights.flights_tickets ADD CONSTRAINT flights_tickets_flight_id_fkey FOREIGN KEY (flight_id) REFERENCES airflights.flights(flight_id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE airflights.flights_tickets ADD CONSTRAINT flights_tickets_ticket_id_fkey FOREIGN KEY (ticket_id) REFERENCES airflights.tickets(ticket_id) ON UPDATE CASCADE ON DELETE RESTRICT;