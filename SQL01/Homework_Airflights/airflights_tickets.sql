-- airflights.tickets definition

-- Drop table

-- DROP TABLE airflights.tickets;

CREATE TABLE airflights.tickets (
	ticket_id serial NOT NULL,
	ticket_code varchar(20) NOT NULL,
	ticket_class int2 NOT NULL,
	ticket_price numeric(6,2) NOT NULL,
	passenger_id int4 NULL,
	CONSTRAINT tickets_pkey PRIMARY KEY (ticket_id),
	CONSTRAINT tickets_ticket_code_key UNIQUE (ticket_code)
);
CREATE INDEX idx_fk_passenger_ticket ON airflights.tickets USING btree (passenger_id);


-- airflights.tickets foreign keys

ALTER TABLE airflights.tickets ADD CONSTRAINT tickets_passenger_id_fkey FOREIGN KEY (passenger_id) REFERENCES airflights.passengers(passenger_id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE airflights.tickets ADD CONSTRAINT tickets_ticket_class_fkey FOREIGN KEY (ticket_class) REFERENCES airflights.travel_classes(travel_class_id) ON UPDATE CASCADE ON DELETE RESTRICT;