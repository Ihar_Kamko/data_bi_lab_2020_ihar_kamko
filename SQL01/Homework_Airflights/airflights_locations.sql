-- airflights.locations definition

-- Drop table

-- DROP TABLE airflights.locations;

CREATE TABLE airflights.locations (
	location_id smallserial NOT NULL,
	city varchar(50) NOT NULL,
	district varchar(50) NULL,
	region varchar(50) NULL,
	country_location int2 NOT NULL,
	CONSTRAINT locations_pkey PRIMARY KEY (location_id)
);
CREATE INDEX idx_fk_location_country ON airflights.locations USING btree (country_location);


-- airflights.locations foreign keys

ALTER TABLE airflights.locations ADD CONSTRAINT locations_country_location_fkey FOREIGN KEY (country_location) REFERENCES airflights.countries(country_id) ON UPDATE CASCADE ON DELETE RESTRICT;