--Create function that inserts new movie with the given name in �film� table. �release_year�, �language� are optional arguments and default to current year and Russian respectively. 
--The function must return film_id of the inserted movie
CREATE OR REPLACE FUNCTION insert_new_movie
	   (ins_title 					text, 
		ins_description 			text DEFAULT NULL, 
		ins_release_year 			year DEFAULT extract(year from current_date), 
		ins_language_id 			int DEFAULT NULL, 
		ins_original_language_id 	int DEFAULT NULL, 
		ins_rental_duration 		int DEFAULT 3, 
		ins_rental_rate 			numeric(4,2) DEFAULT 4.99, 
		ins_length 					int	DEFAULT NULL,
		ins_replacement_cost 		numeric(5,2) DEFAULT 19.99, 
		ins_rating 					mpaa_rating DEFAULT 'G', 
		ins_last_update 			timestamptz DEFAULT now(),  
		ins_special_features 		_text DEFAULT NULL)						--I think that maybe we want to point every or some values...
RETURNS int
LANGUAGE plpgsql 
AS $function$

DECLARE 
	var_film_id 			int;  
	var_language_id 		int;

BEGIN 
	IF ins_language_id IS NOT NULL 
		THEN var_language_id := ins_language_id; 													
	ELSE 
		SELECT l.language_id 
		INTO var_language_id 
		FROM language l																
		WHERE upper(l.name) = 'RUSSIAN';
	END IF;

    IF var_language_id IS NULL 
    THEN
    	INSERT INTO language(name) 
    	VALUES('Russian')
	RETURNING language_id INTO var_language_id;
    END IF;
	
	INSERT INTO film (title, description, release_year, language_id, original_language_id, rental_duration, rental_rate, length, replacement_cost, rating, last_update, special_features)
	VALUES (ins_title, ins_description, ins_release_year, var_language_id, ins_original_language_id, ins_rental_duration, ins_rental_rate, ins_length, ins_replacement_cost, ins_rating, ins_last_update, ins_special_features)
	RETURNING film_id INTO	var_film_id; 
   
	RETURN var_film_id;
END;
$function$;

