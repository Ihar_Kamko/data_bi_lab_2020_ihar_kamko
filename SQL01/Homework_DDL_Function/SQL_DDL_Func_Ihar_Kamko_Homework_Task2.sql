-- What operations do the following functions perform: 

--1) 'film_in_stock' function use input of film_id and store_id, using function 'inventory_in_stock' in condition
--Returns bunch of inventory_id which in stock right now.

--2) 'film_not_in_stock' function use input of film_id and store_id, using function 'inventory_in_stock' in condition. 
--Returns bunch of inventory_id which rented by customer right now.

--3) 'inventory_in_stock' use input of inventory_id.
--1st condition checks rental table for presence of inventory_id, if there are no one - returns TRUE.
--If 1st condition are FALSE 2nd condition checks count where return_date is NULL(rented by customer). If it >0 returning false.
--Returns state of dvd: TRUE - in stock, FALSE - rented.

--4) 'get_customer_balance' function use input of customer_id and date
--Into 1st variable added sum of rental costs of dvd taken by specified customer.
--Into 2nd variable added sum of every overdue returns dvd with penalty equal $1 per day.
--Into 3rd variable added sum of every payments for specified customer.
--Returns balance for specified customer on specified date(indebtedness - negative balance and prepayment - positive balance)

--5) 'inventory_held_by_customer' function use input of inventory id
--Returns customer_id that helds specified film(inventory_id).

--6) 'rewards_report' function use input of minimal count of monthly purchases and minimal amount purchased.
--Function checks input data and displays an error if input data equal 0.
--Then functions gets time period into variable(1 month 3 months ago) use last_day function.
--Creating temprorary table. Declare variable as text array that includes our variables of time period and executes it. So we can get customer_id stored in temporary table and fulfill every conditions.
--Joining temp table on customer table and output every customer infro that fulfill our conditions.
--Drop temporary table.
--Returns every row from customer table that fulfill conditions(deserved a reward).

--7) 'last_day' use input of date.
--It's awesome quibble function. In both conditions we get 1st day of next month using space operator from pg_catalog schema and then subtract 1 day. 
--Returning last day of month that was in input date day 
	

--Why does ‘rewards_report’ function return 0 rows? Correct and recreate the function, so that it's able to return rows properly.
--The function doesn't return any rows because it try to get data from May 2020. If we change last_month_start := CURRENT_DATE - '1 month'::interval; we can get data from previous task.
--And we need set min_monthly_purchases <= 0 and min_dollar_amount_purchased <= 0.00 because we still can input minus values.
CREATE OR REPLACE FUNCTION public.rewards_report(min_monthly_purchases integer, min_dollar_amount_purchased numeric)
 RETURNS SETOF customer
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
DECLARE
    last_month_start DATE;
    last_month_end DATE;
rr RECORD;
tmpSQL TEXT;
BEGIN
    IF min_monthly_purchases <= 0 THEN
        RAISE EXCEPTION 'Minimum monthly purchases parameter must be > 0';
    END IF;
    
    IF min_dollar_amount_purchased <= 0.00 THEN
        RAISE EXCEPTION 'Minimum monthly dollar amount purchased parameter must be > $0.00';
    END IF;

    last_month_start := CURRENT_DATE - '1 month'::interval;
    last_month_start := to_date((extract(YEAR FROM last_month_start) || '-' || extract(MONTH FROM last_month_start) || '-01'),'YYYY-MM-DD');
    last_month_end := LAST_DAY(last_month_start);

    CREATE TEMPORARY TABLE tmpCustomer (customer_id INTEGER NOT NULL PRIMARY KEY);

    tmpSQL := 'INSERT INTO tmpCustomer (customer_id)
        SELECT p.customer_id
        FROM payment AS p
        WHERE DATE(p.payment_date) BETWEEN '||quote_literal(last_month_start) ||' AND '|| quote_literal(last_month_end) || '
        GROUP BY customer_id
        HAVING SUM(p.amount) > '|| min_dollar_amount_purchased || '
        AND COUNT(customer_id) > ' ||min_monthly_purchases ;
    EXECUTE tmpSQL;
   
    FOR rr IN EXECUTE 'SELECT c.* FROM tmpCustomer AS t INNER JOIN customer AS c ON t.customer_id = c.customer_id' LOOP
        RETURN NEXT rr;
    END LOOP;

    tmpSQL := 'DROP TABLE tmpCustomer';
    EXECUTE tmpSQL;

RETURN;
END
$function$
;


-- Is there any function that can potentially be removed from the dvd_rental codebase? If so, which one and why?
--I think that there are 3 functions that can be united in one: film_in_stock, film_not_in_stock and inventory_in_stock
CREATE OR REPLACE FUNCTION public.inventory_status(p_inventory_id integer)
 RETURNS TABLE (status text)
 LANGUAGE plpgsql
AS $function$
DECLARE
    v_rentals INTEGER;
    v_out     INTEGER;
begin
    SELECT count(*) INTO v_rentals
    FROM rental
    WHERE inventory_id = p_inventory_id;

    SELECT COUNT(rental_id) INTO v_out
    FROM inventory LEFT JOIN rental USING(inventory_id)
    WHERE inventory.inventory_id = p_inventory_id
    AND rental.return_date IS NULL;

  RETURN QUERY 
	SELECT
		CASE
		 	WHEN v_rentals = 0 or v_out = 0 THEN 'In stock'
		 	ELSE 'Rented'
		END AS status
FROM inventory i
	WHERE i.inventory_id =p_inventory_id;
 RETURN;
END
$function$
;

--The ‘get_customer_balance’ function describes the business requirements for calculating the client balance. 
--Unfortunately, not all of them are implemented in this function. Try to change function using the requirements from the comments.
 --Not implemented  3) IF A FILM IS MORE THAN RENTAL_DURATION * 2 OVERDUE, CHARGE THE REPLACEMENT_COST
CREATE OR REPLACE FUNCTION public.get_customer_balance(p_customer_id integer, p_effective_date timestamp with time zone)
 RETURNS numeric
 LANGUAGE plpgsql
AS $function$

DECLARE
    v_rentfees DECIMAL(5,2); 
    v_overfees INTEGER;    
    v_replacement_cost NUMERIC(5,2);
    v_payments DECIMAL(5,2);
BEGIN
    SELECT COALESCE(SUM(film.rental_rate),0) INTO v_rentfees
    FROM film, inventory, rental
    WHERE film.film_id = inventory.film_id
      AND inventory.inventory_id = rental.inventory_id
      AND rental.rental_date <= p_effective_date
      AND rental.customer_id = p_customer_id;
     
    SELECT COALESCE(SUM(CASE 
                           WHEN (rental.return_date - rental.rental_date) > (film.rental_duration * '1 day'::interval)
                           THEN EXTRACT(epoch FROM ((rental.return_date - rental.rental_date) - (film.rental_duration * '1 day'::interval)))::INTEGER / 86400 
                           ELSE 0
                        END),0) 
    INTO v_overfees
    FROM rental, inventory, film
    WHERE film.film_id = inventory.film_id
      AND inventory.inventory_id = rental.inventory_id
      AND rental.rental_date <= p_effective_date
      AND rental.customer_id = p_customer_id;
  
    SELECT COALESCE(SUM(CASE 
                           WHEN EXTRACT(day FROM (rental.return_date - rental.rental_date)) > (film.rental_duration *2) THEN film.replacement_cost  --I think that way is possible too
                           ELSE 0
                        END),0) 
    INTO v_replacement_cost
	  FROM rental, inventory, film
    WHERE film.film_id = inventory.film_id
      AND inventory.inventory_id = rental.inventory_id
      AND rental.rental_date <= p_effective_date
      AND rental.customer_id = p_customer_id;
   
    SELECT COALESCE(SUM(payment.amount),0) INTO v_payments
    FROM payment
    WHERE payment.payment_date <= p_effective_date
    AND payment.customer_id = p_customer_id;

    RETURN v_rentfees + v_overfees + v_replacement_cost - v_payments;
END
$function$
;

 

--How do ‘group_concat’ and ‘_group_concat’ functions work? (database creation script might help) Where are they used?
--'group concat' is created aggregate function which is analog to group_concat mysql function. It's works same to other aggregate function, uses '_group_concat' function and output comma separated text value or column.
--'_group_concat' function joins 2 values to comma separated. If one of value is NULL the result will be other value. Output comma separated text value or column

--What does ‘last_updated’ function do? Where is it used?
--'last_updated' it's a trigger procedure that changes last_update column to CURRENT_TIMESTAMP for every UPDATE command.
--It used in triggers of every table in our database that execute it before updating rows. Function update last_update column for updating rows.

--What is tmpSQL variable for in 'rewards_report' function? Can this function be recreated without EXECUTE statement and dynamic SQL? Why?	
--Variable tmpSQL is created for saving query as text and used by 'rewards_report' function with EXECUTE statement to fill temporary table with data that fulfill query conditions.

--Can this function be recreated without EXECUTE statement and dynamic SQL? Why?
--Function can be recreated with using RETURN QUERY
CREATE OR REPLACE FUNCTION public.rewards_report(min_monthly_purchases integer, min_dollar_amount_purchased numeric)
 RETURNS SETOF customer
 LANGUAGE plpgsql
 SECURITY DEFINER
AS $function$
DECLARE
    last_month_start DATE;
    last_month_end DATE;
BEGIN
    IF min_monthly_purchases <= 0 THEN
        RAISE EXCEPTION 'Minimum monthly purchases parameter must be > 0';
    END IF;
    
    IF min_dollar_amount_purchased <= 0.00 THEN
        RAISE EXCEPTION 'Minimum monthly dollar amount purchased parameter must be > $0.00';
    END IF;

    last_month_start := CURRENT_DATE - '1 month'::interval;
    last_month_start := to_date((extract(YEAR FROM last_month_start) || '-' || extract(MONTH FROM last_month_start) || '-01'),'YYYY-MM-DD');
    last_month_end := LAST_DAY(last_month_start);

	RETURN QUERY 
		SELECT * 
		FROM customer 
		WHERE customer_id  IN 
			(SELECT p.customer_id
        	FROM payment AS p 
        	WHERE DATE(p.payment_date) BETWEEN last_month_start  AND last_month_end
        	GROUP BY p.customer_id
        	HAVING sum(p.amount) > min_dollar_amount_purchased
        	AND count(p.customer_id) > min_monthly_purchases);
END
$function$
;
