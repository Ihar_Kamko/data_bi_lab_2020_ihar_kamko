--Build a query to create sales report that shows sales distribution by gender (F / M), marital status and by age (21-30 / 31-40 / 41-50 / 51-60 years).
--Calculate the total sales based on the marital status of the customers
WITH abc AS (
SELECT cust_gender, cust_marital_status, 
	sum(CASE WHEN age BETWEEN '21' AND '30' THEN amount_sold ELSE 0 END) AS twenty,
	sum(CASE WHEN age BETWEEN '31' AND '40' THEN amount_sold ELSE 0 END) AS thirty,
	sum(CASE WHEN age BETWEEN '41' AND '50' THEN amount_sold ELSE 0 END) AS forty,
	sum(CASE WHEN age BETWEEN '51' AND '60' THEN amount_sold ELSE 0 END) AS fifty
FROM 
	(SELECT cust_gender, lower(c.cust_marital_status) AS cust_marital_status, s.amount_sold, t.calendar_year - c.cust_year_of_birth AS age 
	FROM sales s
		INNER JOIN customers c on s.cust_id =c.cust_id 
		INNER JOIN times t on s.time_id=t.time_id 
	WHERE t.calendar_year ='2000'
	AND lower(cust_marital_status) IN ('single','married')) foo
GROUP BY cust_gender, cust_marital_status)

SELECT cust_gender, cust_marital_status, to_char(twenty, '99999999.99') AS "21-30", to_char(thirty, '99999999.99') AS "31-40", to_char(forty, '99999999.99') AS "41-50", to_char(fifty, '99999999.99') AS "51-60"
FROM abc 
UNION 
SELECT null, 'Total for married' AS cust_marital_status, to_char(sum(twenty),'99999999.99'), to_char(sum(thirty),'99999999.99'), to_char(sum(forty),'99999999.99'), to_char(sum(fifty),'99999999.99')
FROM abc
WHERE cust_marital_status = 'married'
UNION 
SELECT null, 'Total for single' AS cust_marital_status, to_char(sum(twenty),'99999999.99'), to_char(sum(thirty),'99999999.99'), to_char(sum(forty),'99999999.99'), to_char(sum(fifty),'99999999.99')
FROM abc
WHERE cust_marital_status = 'single'
ORDER BY cust_gender;

--Create an annual sales report broken down by calendar years and months. Annual sales for January of each year are presented in monetary units ($),
--the remaining months should contain the dynamics of sales in percent relative to January.
SELECT *
FROM crosstab(
$$
SELECT t.calendar_year, t.calendar_month_number, 
	CASE 
		WHEN t.calendar_month_number = 1 THEN to_char(sum(amount_sold), '99999999.99')
		ELSE to_char(round((sum(amount_sold) - (first_value(sum(amount_sold)) OVER w))*100/first_value(sum(amount_sold)) OVER w, 2), '99999999.99')
	END AS diff_percent
FROM sales s
	INNER JOIN times t ON s.time_id =t.time_id 
GROUP BY t.calendar_year, t.calendar_month_number
WINDOW w AS (PARTITION BY t.calendar_year ORDER BY t.calendar_month_number ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW EXCLUDE CURRENT ROW)
$$) AS 	abc(calendar_year int2, "Jan $" text, "Feb %" text, "Mar %" text, "Apr %" text, "May %" text, "Jun %" text, "Jul %" text, "Aug %" text, "Sep %" text, "Oct %" text, "Nov %" text, "Dec %" text);
	
--Which products were the most expensive (MAX (Costs.Unit_Price)) in their product category each year (1998-2001)
WITH abc AS (
	SELECT t.calendar_year, p.prod_category, p.prod_name, max(c.unit_price) as max_price,
			rank() OVER (PARTITION BY t.calendar_year, p.prod_category ORDER BY max(c.unit_price) DESC) AS rank
FROM sales s
	INNER JOIN products p ON s.prod_id =p.prod_id
	INNER JOIN times t ON s.time_id =t.time_id 
	INNER JOIN costs c ON p.prod_id=c.prod_id AND t.time_id=c.time_id 	
GROUP BY t.calendar_year, p.prod_category, p.prod_name)

SELECT  prod_name
FROM abc 
WHERE rank =1
GROUP BY prod_name
HAVING count(prod_name)=4;


