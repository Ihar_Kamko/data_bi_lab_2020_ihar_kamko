CREATE DATABASE  IF NOT EXISTS `heavy_metallic_store` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `heavy_metallic_store`;
-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: heavy_metallic_store
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `musicians`
--

DROP TABLE IF EXISTS `musicians`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `musicians` (
  `musician_id` smallint unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `date_of_birth` date NOT NULL,
  `nationality_id` smallint unsigned NOT NULL,
  `role_id` smallint unsigned NOT NULL,
  PRIMARY KEY (`musician_id`),
  KEY `role_id_idx` (`role_id`),
  KEY `nationality_id_idx` (`nationality_id`),
  CONSTRAINT `nationality_id` FOREIGN KEY (`nationality_id`) REFERENCES `nationalities` (`nationality_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_id` FOREIGN KEY (`role_id`) REFERENCES `musicians_roles` (`musician_role_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `musicians`
--

LOCK TABLES `musicians` WRITE;
/*!40000 ALTER TABLE `musicians` DISABLE KEYS */;
INSERT INTO `musicians` VALUES (1,'Фредди','Меркьюри','0000-00-00',1,1),(2,'Брайан ','Мэй','0000-00-00',1,5),(3,'Роджер ','Тейлор','0000-00-00',1,7),(4,'Джон','Дикон','0000-00-00',1,6),(5,'Тилль','Линдерманн','0000-00-00',1,1),(6,'Рихард','Круспе','0000-00-00',1,5),(7,'Пауль','Ландерс','0000-00-00',1,3),(8,'Оливер','Ридель','0000-00-00',1,6),(9,'Кристоф','Шнайдер','0000-00-00',1,7),(10,'Кристиан','Лоренц','0000-00-00',1,4),(11,'Курт','Кобейн','0000-00-00',1,1),(12,'Крист','Новоселич','0000-00-00',1,6),(13,'Дэйв','Грол','0000-00-00',1,7),(14,'Джон','Леннон','0000-00-00',1,1),(15,'Пол','Маккартни','0000-00-00',1,6),(16,'Джордж','Харрисон','0000-00-00',1,5),(17,'Ринго','Старр','0000-00-00',1,7),(18,'Ангус','Янг','0000-00-00',1,2),(19,'Крис','Слэйд','0000-00-00',1,7),(20,'Стиви ','Янг','0000-00-00',1,3),(21,'Брайан','Джонсон','0000-00-00',1,1),(22,'Джеймс','Хэтфилд','0000-00-00',1,1),(23,'Ларс','Ульрих','0000-00-00',1,7),(24,'Кирк','Хэммет','0000-00-00',1,5),(25,'Роберт','Трухильо','0000-00-00',1,6),(26,'Ник','Мейсон','0000-00-00',1,7),(27,'Роджер','Уотерс','0000-00-00',1,6),(28,'Ричард','Райт','0000-00-00',1,4),(29,'Сид ','Барретт','0000-00-00',1,1),(30,'Дэвид','Гилмор','0000-00-00',1,5),(31,'Клаус','Майне','0000-00-00',1,1),(32,'Рудольф','Шенкер','0000-00-00',1,5),(33,'Маттиас','Ябс','0000-00-00',1,5),(34,'Микки','Ди','0000-00-00',1,7),(35,'Павел','Мончивода','0000-00-00',1,6),(36,'Роберт','Плант','0000-00-00',1,1),(37,'Джимми','Пейдж','0000-00-00',1,5),(38,'Джон','Пол Джон','0000-00-00',1,6),(39,'Джон','Бонэм','0000-00-00',1,7),(40,'Мик','Джаггер','0000-00-00',1,1),(41,'Кит','Ричардс','0000-00-00',1,5),(42,'Чарли','Уоттс','0000-00-00',1,7),(43,'Ронни','Вуд','0000-00-00',1,6),(44,'Джин','Симмонс','0000-00-00',1,6),(45,'Пол','Стэнли','0000-00-00',1,1),(46,'Эрик','Сингер','0000-00-00',1,7),(47,'Томми','Тайер','0000-00-00',1,5),(48,'Эксл','Роуз','0000-00-00',1,1),(49,'Джафф','Маккаган','0000-00-00',1,6),(50,'Сол','Хадсон','0000-00-00',1,2),(51,'Диззи','Рид','0000-00-00',1,4),(52,'Ричард','Фортус','0000-00-00',1,3),(53,'Фрэнк','Феррер','0000-00-00',1,7),(54,'Мелисса','Риз','0000-00-00',1,4),(55,'Стивен','Тайлер','0000-00-00',1,1),(56,'Джо','Перри','0000-00-00',1,5),(57,'Том','Хэмилтон','0000-00-00',1,6),(58,'Джоуи','Крамер','0000-00-00',1,7),(59,'Брэд','Уитфорд','0000-00-00',1,5),(60,'Джим','Моррисон','0000-00-00',1,1),(61,'Рэй','Манзарек','0000-00-00',1,4),(62,'Робби','Кригер','0000-00-00',1,5),(63,'Джон','Денсмор','0000-00-00',1,7),(64,'Пол','Хьюстон','0000-00-00',1,1),(65,'Дэвид','Эванс','0000-00-00',1,5),(66,'Адам','Клейтон','0000-00-00',1,6),(67,'Ларри','Маллен','0000-00-00',1,7);
/*!40000 ALTER TABLE `musicians` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-20  8:49:03
