CREATE DATABASE  IF NOT EXISTS `heavy_metallic_store` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `heavy_metallic_store`;
-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: heavy_metallic_store
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `musicians_instuments`
--

DROP TABLE IF EXISTS `musicians_instuments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `musicians_instuments` (
  `musician_id` smallint unsigned NOT NULL,
  `instrument_id` smallint unsigned NOT NULL,
  PRIMARY KEY (`musician_id`,`instrument_id`),
  KEY `instruments_idx` (`instrument_id`) /*!80000 INVISIBLE */,
  KEY `musicians_idx` (`musician_id`),
  CONSTRAINT `instruments` FOREIGN KEY (`instrument_id`) REFERENCES `instruments` (`instrument_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `musicians` FOREIGN KEY (`musician_id`) REFERENCES `musicians` (`musician_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `musicians_instuments`
--

LOCK TABLES `musicians_instuments` WRITE;
/*!40000 ALTER TABLE `musicians_instuments` DISABLE KEYS */;
INSERT INTO `musicians_instuments` VALUES (1,1),(1,7),(1,9),(2,2),(2,7),(2,8),(3,5),(4,6),(4,7),(5,1),(6,4),(6,8),(7,3),(7,8),(8,6),(9,5),(10,2),(11,1),(11,7),(12,6),(13,5),(13,8),(14,1),(14,2),(14,6),(14,7),(14,8),(15,1),(15,2),(15,5),(15,6),(15,7),(15,8),(16,1),(16,6),(16,7),(16,8),(17,1),(17,5),(17,8),(18,4),(19,5),(20,3),(20,6),(20,8),(21,1),(22,1),(22,3),(23,5),(24,4),(24,8),(25,6),(25,8),(26,5),(27,1),(27,3),(27,6),(28,1),(28,2),(29,1),(29,3),(29,4),(30,1),(30,2),(30,3),(30,4),(30,6),(31,1),(32,3),(32,8),(33,4),(33,8),(34,5),(35,6),(35,8),(36,1),(37,7),(38,2),(38,6),(39,5),(40,1),(40,2),(40,3),(40,8),(41,1),(41,3),(41,4),(41,6),(42,5),(42,8),(43,3),(43,4),(43,6),(43,8),(44,1),(44,6),(44,8),(45,1),(45,3),(45,8),(46,1),(46,5),(46,8),(47,1),(47,4),(47,8),(48,1),(48,9),(49,6),(49,8),(50,3),(50,4),(50,8),(51,2),(51,8),(51,9),(52,3),(52,4),(52,8),(53,5),(54,2),(54,8),(54,10),(55,1),(55,2),(56,7),(56,8),(57,6),(58,5),(59,7),(60,1),(61,1),(61,2),(61,11),(62,1),(62,7),(63,5),(63,8),(64,1),(64,2),(64,3),(65,1),(65,2),(65,4),(65,10),(66,6),(67,5);
/*!40000 ALTER TABLE `musicians_instuments` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-20  8:49:02
