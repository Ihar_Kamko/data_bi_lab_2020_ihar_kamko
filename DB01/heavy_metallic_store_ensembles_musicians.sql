CREATE DATABASE  IF NOT EXISTS `heavy_metallic_store` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `heavy_metallic_store`;
-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: heavy_metallic_store
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ensembles_musicians`
--

DROP TABLE IF EXISTS `ensembles_musicians`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ensembles_musicians` (
  `ensemble_id` smallint unsigned NOT NULL,
  `musician_id` smallint unsigned NOT NULL,
  PRIMARY KEY (`ensemble_id`,`musician_id`),
  KEY `musician_id_idx` (`musician_id`) /*!80000 INVISIBLE */,
  KEY `ensembles_id_idx` (`ensemble_id`),
  CONSTRAINT `ensemble_id` FOREIGN KEY (`ensemble_id`) REFERENCES `ensembles` (`ensemble_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `musician_id` FOREIGN KEY (`musician_id`) REFERENCES `musicians` (`musician_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ensembles_musicians`
--

LOCK TABLES `ensembles_musicians` WRITE;
/*!40000 ALTER TABLE `ensembles_musicians` DISABLE KEYS */;
INSERT INTO `ensembles_musicians` VALUES (1,1),(1,2),(1,3),(1,4),(2,5),(2,6),(2,7),(2,8),(2,9),(2,10),(3,11),(3,12),(3,13),(4,14),(4,15),(4,16),(4,17),(5,18),(5,19),(5,20),(5,21),(6,22),(6,23),(6,24),(6,25),(7,26),(7,27),(7,28),(7,29),(7,30),(8,31),(8,32),(8,33),(8,34),(8,35),(9,36),(9,37),(9,38),(9,39),(10,40),(10,41),(10,42),(10,43),(11,44),(11,45),(11,46),(11,47),(12,48),(12,49),(12,50),(12,51),(12,52),(12,53),(12,54),(13,55),(13,56),(13,57),(13,58),(13,59),(14,60),(14,61),(14,62),(14,63),(15,64),(15,65),(15,66),(15,67);
/*!40000 ALTER TABLE `ensembles_musicians` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-20  8:49:03
