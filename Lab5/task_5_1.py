import re
import argparse

#function for insert data into dictionary
def output_insert(table, column, type):
    for abc in range(len(column)):
        column_name = column[abc]
        column_type = type[abc]
        part = dict()
        part[column_name] = column_type
        if table in output:
            if part in output[table]:
                continue
            output[table].append(part)
        else:
            output[table] = [part]

parser = argparse.ArgumentParser(description='')
parser.add_argument('x', type=str)
file_path = parser.parse_args()

#searching table's arrays
try:
    with open(file_path.x) as file_object:
        input = file_object.read().split('+-')
        founded_arrays = []
        for line in input:
            if re.search('Scan', line):
                founded_arrays.append(line)

#searching data with regular expressions
    output = {}
    for line in founded_arrays:
        tables = re.findall(".*?\((.*?)\)", line)
        tables = tables[0]
    
        columns = re.findall("\$\d*\:(.*?)\[\S*\]", line)
    
        types = re.findall("\$\d*\:\S*\[(.*?)\]", line)
#data insert in dictionary with fuction
        output_insert(tables, columns, types)


#output sorted line by line for clarity

    list_keys = list(output.keys()) 
    list_keys.sort()
    for i in list_keys:
        print(i, ':', output[i])
except FileNotFoundError:
        print('Wrong filepath!')

    